// Server Side Code: TestUDPServer.java
// Syntax:  java TestUDPServer  port

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class TestUDPServer
{
    public static void main(String s[]) throws IOException
    {
        try
        {
                DatagramSocket socket = new DatagramSocket(Integer.valueOf(s[0]).intValue());
                byte[] buf = new byte[256];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                System.out.println("Waiting package");
                socket.receive(packet);
                System.out.println("Package received. Send information back");
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                System.out.println("Reply information:" + address.toString() + "port:" + packet.getPort());
                packet = new DatagramPacket(buf, buf.length, address, port);
                socket.send(packet);
        }
        catch (SocketException e)
        {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }
}