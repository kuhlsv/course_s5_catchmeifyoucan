package de.hsf.g4.catchmeifyoucan.wlan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

public class WlanServer extends Thread {

    private final String TAG = "WlanServer";
    private ArrayList<ServerThread> sThreads = new ArrayList<>();
    private ServerSocket server = null;
    private WlanResponseHandler rh = new WlanResponseHandler();

    private int port;

    public WlanServer(int port) {
        this.port = port;
        rh.setServer(this);
    }

    public void run() {
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            Log.e(TAG,"couldn't create Serversocket!");
            e.printStackTrace();
        }
        Socket s;

        if(server != null) {
            while (true) {
                try {
                    Log.i(TAG, "looking for client...");
                    s = server.accept();
                    ServerThread t = new ServerThread(s);
                    t.start();
                    Log.i(TAG, "found a client!");
                    sThreads.add(t);
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class ServerThread extends Thread {
        private Socket s;
        private BufferedWriter out;
        private BufferedReader in;
        private ServerThread(Socket s) {
            this.s = s;
        }
        public void run() {
            try {
                // read
                in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                // test echo
                out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));

                // listener
                String fromUser;
                while ((fromUser = in.readLine()) != null) {
                    rh.serverHandle(fromUser);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @SuppressLint({"StaticFieldLeak"})
        private void write(String msg) {
            AsyncTask<String, Void, Void> send = new AsyncTask<String, Void, Void>() {

                @Override
                protected Void doInBackground(String... strings) {
                    try {
                        out.write(strings[0]);
                        out.newLine();
                        out.flush();
                        //Log.d(TAG + " sent", "'" + strings[0] + "' to " + s.getInetAddress().toString());
                    } catch(java.io.IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            send.execute(msg);
        }
    }

    public void sendToAllClients(String msg) {
        Log.i(TAG, "sending message to " + sThreads.size() + " clients");
        for(ServerThread s : sThreads) {
            s.write(msg);
        }
    }
}
