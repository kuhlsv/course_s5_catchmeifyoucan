package de.hsf.g4.catchmeifyoucan.game;

import android.graphics.Color;

public class GameProperties {


    /**********************
     *  Dimensions
     **********************/

    /*
    * All these are relative sizes. These are no sizes in pixel. The display class will calculate
    * the actual pixel size.
    * */

    public static final int ASPECT_RATION_WIDTH = 16;
    public static final int ASPECT_RATION_HEIGHT = 9;
    public static final int MAP_WIDTH = 1600;
    public static final int MAP_HEIGHT = 900;
    public static final int PLAYER_SIZE = 30;
    public static final int PLAYER_MAX_SPEED = 20;
    public static final int PLAYER_MAX_SPEED_FREE_TURN = 3;
    public static final int PLAYER_MAX_TURN_RADIUS = 10;
    public static final int AMOUNT_OF_OBSTACLE = 20;
    public static final int MARGIN_OF_OBSTACLE = 70;
    public static final int OBSTACLE_WALL_MAX_LENGTH = 400;
    public static final int OBSTACLE_WALL_MIN_LENGTH = 100;
    public static final int OBSTACLE_WALL_THICKNESS = 20;
    public static final int PRISON_WALL_LENGTH = 150;
    public static final int PRISON_WALL_THICKNESS = 20;
    public static final int PRISON_X = 0;
    public static final int PRISON_Y = 0;
    public static final int HUNTER_START_X = PRISON_X + PRISON_WALL_LENGTH;
    public static final int HUNTER_START_Y = PRISON_Y + PRISON_WALL_LENGTH;
    public static final int HUNTED_START_X = MAP_WIDTH - PLAYER_SIZE;
    public static final int HUNTED_START_Y = MAP_HEIGHT - PLAYER_SIZE;

    public static final int GAMETIME = 60;
    public static final int FPS = 16;

    /**********************
     *  Colors
     **********************/

    public static final int BACKGROUND_COLOR = Color.WHITE;
    public static final int MAP_COLOR = Color.GRAY;
    public static final int OWN_PLAYER_COLOR = Color.RED;
    public static final int HUNTER_PLAYER_COLOR = Color.WHITE;
    public static final int HUNTED_PLAYER_COLOR = Color.BLACK;
    public static final int OBSTACLE_WALL_HORIZONTAL_COLOR = Color.rgb(65, 160, 150);
    public static final int OBSTACLE_WALL_VERTICAL_COLOR = Color.rgb(87, 160, 65);
    public static final int OBSTACLE_CROSS_COLOR = Color.rgb(134, 65, 160);
    public static final int OBSTACLE_CORNER_COLOR = Color.rgb(226, 204, 31);
    public static final int PRISON_COLOR = Color.rgb(244, 122, 66);

    // Dark THEME

    public static final int BACKGROUND_COLOR_DARK = Color.BLACK;
    public static final int MAP_COLOR_DARK = Color.DKGRAY;

    public static final int OWN_PLAYER_COLOR_DARK = Color.RED;
    public static final int HUNTER_PLAYER_COLOR_DARK = Color.WHITE;
    public static final int HUNTED_PLAYER_COLOR_DARK = Color.GREEN;

    public static final int OBSTACLE_WALL_HORIZONTAL_COLOR_DARK = Color.WHITE;
    public static final int OBSTACLE_WALL_VERTICAL_COLOR_DARK = Color.WHITE;
    public static final int OBSTACLE_CROSS_COLOR_DARK = Color.WHITE;
    public static final int OBSTACLE_CORNER_COLOR_DARK = Color.WHITE;
    public static final int PRISON_COLOR_DARK = Color.RED;

    //luminous intensity border for chancing theme
    public static final float ILLUMINANCE_LIMIT = 40;
}
