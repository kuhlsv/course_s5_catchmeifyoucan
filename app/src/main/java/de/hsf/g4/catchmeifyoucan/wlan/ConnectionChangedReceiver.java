package de.hsf.g4.catchmeifyoucan.wlan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.widget.Toast;

public class ConnectionChangedReceiver extends BroadcastReceiver {

    private static final String TAG = ConnectionChangedReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        // Wifi
        int wifiStateExtra = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                WifiManager.WIFI_STATE_UNKNOWN);
        int duration = Toast.LENGTH_SHORT;
        CharSequence text;
        Toast toast;
        switch (wifiStateExtra) {
            case WifiManager.WIFI_STATE_ENABLING:
                text = "Wifi is enabled!";
                toast = Toast.makeText(context, text, duration);
                toast.show();
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_DISABLED:
                if(!Utils.checkWlan(context)){
                    // Info
                    text = "Please enable Wlan!";
                    toast = Toast.makeText(context, text, duration);
                    toast.show();
                    // Force to setting
                    Utils.forceSettings(context);
                }
                break;
        }
    }
}
