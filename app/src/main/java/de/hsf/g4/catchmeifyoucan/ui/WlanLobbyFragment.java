package de.hsf.g4.catchmeifyoucan.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.GameActivity;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.wlan.BroadcastService;

public class WlanLobbyFragment extends Fragment {

    private static final String TAG = WlanLobbyFragment.class.getSimpleName();
    private DAO dao = DAO.getInstance();
    private ArrayList<Player> players = new ArrayList<>();
    // List to show in view
    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayAdapter<String> listAdapter;
    private Boolean allBeReady = false;
    private Boolean isHunterChoosed = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wlan_lobby, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Reset self player
        dao.getSelf().setPlayerRole(Player.PlayerRole.HUNTED);
        dao.getSelf().setReady(false);
        if(dao.getSelf().getHost()){
            dao.getSelf().setReady(true);
        }
        // add self to dao playerlist
        ArrayList<Player> pl = new ArrayList<>();
        pl.add(dao.getSelf());
        dao.setPlayers(pl);
        dao.updatePlayer(dao.getSelf());
        // Create List
        ListView listJoin = view.findViewById(R.id.lv_player);
        listAdapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                R.layout.list_item,
                listItems);
        listJoin.setAdapter(listAdapter);
        // Show player info
        TextView info = view.findViewById(R.id.tv_playerInfo);
        info.setText(dao.getSelf().getName());
        // Create event listener
        View.OnClickListener setPlayerRoleHunted = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player player = DAO.getInstance().getSelf();
                player.setPlayerRole(Player.PlayerRole.HUNTED);
                dao.updatePlayer(player);
                view.findViewById(R.id.buttonRole2).setBackgroundColor(getResources().getColor(R.color.buttonDarkClicked));
                view.findViewById(R.id.buttonRole1).setBackgroundColor(getResources().getColor(R.color.buttonDark));
            }
        };
        View.OnClickListener setPlayerRoleHunter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player player = DAO.getInstance().getSelf();
                player.setPlayerRole(Player.PlayerRole.HUNTER);
                dao.updatePlayer(player);
                view.findViewById(R.id.buttonRole1).setBackgroundColor(getResources().getColor(R.color.buttonDarkClicked));
                view.findViewById(R.id.buttonRole2).setBackgroundColor(getResources().getColor(R.color.buttonDark));
            }
        };
        View.OnClickListener startGameListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isHunterChoosed){
                    if(allBeReady){
                        if(players.size() <= 1){
                            Toast.makeText(getActivity(), "You need at least to be two!", Toast.LENGTH_SHORT).show();
                        }else{
                            dao.startGame(); // Start Game for all Players
                        }
                    }else{
                        Toast.makeText(getActivity(), "Some players are not ready!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "You need a Hunter!", Toast.LENGTH_SHORT).show();
                }
            }
        };
        View.OnClickListener readyGameListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btnStart = view.findViewById(R.id.btn_start);
                if (btnStart.getText().equals("I'm not ready.")){
                    setReady(true);
                    btnStart.setText("I'm ready!");
                    btnStart.setBackgroundColor(Color.rgb(20,120,20));
                }else {
                    setReady(false);
                    btnStart.setText("I'm not ready.");
                    btnStart.setBackgroundColor(Color.rgb(120,20,20));
                }
            }
        };
        // Create button
        Button btnHunter = view.findViewById(R.id.buttonRole1);
        btnHunter.setOnClickListener(setPlayerRoleHunter);
        Button btnHunted = view.findViewById(R.id.buttonRole2);
        btnHunted.setOnClickListener(setPlayerRoleHunted);
        Button btnStart = view.findViewById(R.id.btn_start);
        // Add players to list
        if(players != null){
            updatePlayerlist();
        }
        // Enable Settings for host
        Player player = DAO.getInstance().getSelf();
        if(player.getHost()){
            // Add process bar for sending broadcast
            view.findViewById(R.id.pb_sendBroadcast).setVisibility(View.VISIBLE);
            // Host can start game
            btnStart.setOnClickListener(startGameListener);
        }else{
            // Player can be ready
            btnStart.setText("I'm not ready.");
            btnStart.setBackgroundColor(Color.rgb(120,20,20));
            btnStart.setOnClickListener(readyGameListener);
        }
    }

    private void updatePlayerlist(){
        Boolean allReady = true;
        Boolean isHunterThere = false;
        if(listItems.size() > 0){
            listAdapter.clear();
        }
        if(players.size() > 0){
            for(int i = 0; i < players.size(); i++){
                listAdapter.add(players.get(i).toString().replace("null","ROLE"));
                if(!players.get(i).isReady()){
                    allReady = false;
                }
                if(players.get(i).getPlayerRole() != null && players.get(i).getPlayerRole() == Player.PlayerRole.HUNTER){
                    isHunterThere = true;
                }
            }
        }
        if(dao.getSelf().getHost()){
            if(isHunterThere){
                isHunterChoosed = true;
            }else{
                isHunterChoosed = false;
            }
            try{
                if(allReady){
                    allBeReady = true;
                    Button btnStart = null;
                    if(getView() != null){
                        btnStart = getView().findViewById(R.id.btn_start);
                    }
                    if(players.size()>1 && btnStart != null){
                        btnStart.setBackgroundColor(Color.rgb(20,120,20));
                    }
                }else{
                    allBeReady = false;
                    Button btnStart = getView().findViewById(R.id.btn_start);
                    btnStart.setBackgroundColor(Color.rgb(120,20,20));
                }
            }catch (Exception e){
                Log.e(TAG, e.getMessage());
            }
        }
    }

    // Switch to Game Activity
    // Give Player (self) and Playerlist (Joined) to game
    public void startGame(Context context){
        Intent i = new Intent(context, BroadcastService.class);
        getActivity().getApplicationContext().stopService(i);
        Intent intent = new Intent(getActivity(), GameActivity.class);
        getActivity().startActivity(intent);
    }

    public void setReady(Boolean ready){
        dao.getSelf().setReady(ready);
        dao.updatePlayer(dao.getSelf());
    }

    @Override
    public void onResume() {
        super.onResume();
        // create dao data checking loop
        dao.setStarted(false);
        final Handler handler = new Handler();
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                try {
                    //dao.getPlayers() != players
                    players = dao.getPlayers();
                    // playerlist updated
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            updatePlayerlist();
                        }
                    }, 1000);
                    if (dao.isStarted()) {
                        // game start
                        Log.i(TAG, "START");
                        startGame(getContext());
                        this.cancel();
                    }
                }catch (Exception e){
                    Log.e(TAG, e.getMessage());
                }
            }
        },0,100);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}