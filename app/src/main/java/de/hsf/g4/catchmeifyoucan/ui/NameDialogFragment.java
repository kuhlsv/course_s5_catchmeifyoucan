package de.hsf.g4.catchmeifyoucan.ui;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.Player;

public class NameDialogFragment extends DialogFragment {

    private static final String TAG = NameDialogFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View dv = inflater.inflate(R.layout.fragment_dialog, container, false);
        // Create event listener
        View.OnClickListener cancel = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                Toast.makeText(getActivity(),"Canceled", Toast.LENGTH_LONG).show();
            }
        };
        View.OnClickListener create =  new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String name;
                try{
                    name = ((EditText) dv.findViewById(R.id.et_playername)).getText().toString();
                    if(name.contains(" ") || name.contains("_")){
                        throw new Exception("Invalid characters!");
                    }
                    Player player = DAO.getInstance().getSelf();
                    if(!name.equals("")){
                        player.setName(name);
                        player.setNameIsSet(true);
                        // Save player
                        DAO.getInstance().saveSelf(getContext());
                    }
                    Toast.makeText(getActivity(),"New Name: " + name, Toast.LENGTH_LONG).show();
                }catch(Exception e){
                    Log.e(TAG, "Bad Input. " + e.getMessage());
                    Toast.makeText(getActivity(),"Canceled", Toast.LENGTH_LONG).show();
                }
                getDialog().dismiss();
            }
        };
        // Twoplayer button
        Button btnCancel = dv.findViewById(R.id.buttonCancel);
        btnCancel.setOnClickListener(cancel);
        // Multiplayer button
        Button btnCreate = dv.findViewById(R.id.buttonCreate);
        btnCreate.setOnClickListener(create);
        return dv;
    }

}