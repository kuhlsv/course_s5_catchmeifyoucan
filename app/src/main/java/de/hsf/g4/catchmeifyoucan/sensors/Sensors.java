package de.hsf.g4.catchmeifyoucan.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;

public class Sensors implements SensorEventListener {

    public static final String TAG = Sensors.class.getSimpleName();
    private SensorManager sensorManager;

    private Sensor lightSensor;
    private Sensor rotationVectorSensor;
    private float x;
    private float y;
    private float lux;
    private boolean rotationVectorSensosrIsAvailable = false;

    public Sensors(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        checkRotationVectorSensorAvailability();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            getRotationData(sensorEvent);
        }
        if(sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
            getLightData(sensorEvent);
        }
    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {
    }

    public void instanceSensors() {
        if(rotationVectorSensosrIsAvailable) {
            rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            // Register the listener
            sensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        // Register the listener
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    private void checkRotationVectorSensorAvailability() {
        // List of all Sensors Available
        List<Sensor> SensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        for (int i=0; i<SensorList.size(); i++){
            Sensor tmp = SensorList.get(i);
            // 11 -> TYPE_ROTATION_VECTOR
            if(tmp.getType() == 11) {
                rotationVectorSensosrIsAvailable = true;
                break;
            }
        }
    }

    private void getRotationData(SensorEvent sensorEvent) {
        float[] rotationMatrix = new float[16];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, sensorEvent.values);

        // Remap coordinate system
        float[] remappedRotationMatrix = new float[16];
        SensorManager.remapCoordinateSystem(rotationMatrix,
                SensorManager.AXIS_X,
                SensorManager.AXIS_Y,
                remappedRotationMatrix);

        // Convert to orientations
        float[] orientations = new float[3];
        SensorManager.getOrientation(remappedRotationMatrix, orientations);

        // Umrechnung der Werte von Radiant in Grad
        for(int i = 0; i < 3; i++) {
            orientations[i] = (float)(Math.toDegrees(orientations[i]));
        }
        // Wenn das Handy flach , quer liegt
        // orientations[0] -> 90
        // orientations[1] -> 0
        // orientations[2] -> 0

        // orientations[1] -> x
        // orientations[2] -> y
        this.x = orientations[1];
        this.y = orientations[2];
    }

    private void getLightData(SensorEvent sensorEvent) {
        this.lux = sensorEvent.values[0];
    }

    public float[] getOrientationValues(){
        float[] values = new float[2];
        values[0] = this.x;
        values[1] = this.y;
        return values;
    }

    public float getIlluminance() {
        return this.lux;
    }

    public boolean getRotationVectorSensosrIsAvailability() {
        return rotationVectorSensosrIsAvailable;
    }
}
