package de.hsf.g4.catchmeifyoucan.game;

import android.graphics.Point;
import android.util.Log;

import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_HEIGHT;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_WIDTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PLAYER_SIZE;

public class Crasher {

    static String TAG = "Crasher";

    public static boolean isCrashedObstacles(AtomicObstacle obstacle1, AtomicObstacle obstacle2) {
        return isCrashedObstacles(obstacle1,obstacle2,0);
    }

    public static boolean isCrashedObstacles(AtomicObstacle obstacle1, AtomicObstacle obstacle2, int marginOfObstacle) {
        return isCrashed(obstacle1,obstacle2,marginOfObstacle);
    }

    public static boolean isCrashedObstaclePlayer(AtomicObstacle obstacle, Point player){
        if(player.x < 0 || player.x > MAP_WIDTH || player.y < 0 || player.y > MAP_HEIGHT ||
                player.x + PLAYER_SIZE < 0 || player.x + PLAYER_SIZE > MAP_WIDTH || player.y + PLAYER_SIZE< 0 || player.y + PLAYER_SIZE> MAP_HEIGHT) {
            return true;
        } else {
            return isCrashed(obstacle, new AtomicObstacle(player.x, player.y, player.x + PLAYER_SIZE, player.y + PLAYER_SIZE), 0);
        }
    }

    public static boolean isCrashedPlayerPlayer(Point player1, Point player2){
        return isCrashed(new AtomicObstacle(player1.x, player1.y, player1.x + PLAYER_SIZE, player1.y + PLAYER_SIZE), new AtomicObstacle(player2.x, player2.y, player2.x + PLAYER_SIZE, player2.y + PLAYER_SIZE), 0);
    }

    private static boolean isCrashed(AtomicObstacle obstacle1, AtomicObstacle obstacle2, int marginOfObstacle) {
        //assign who is the smaller side
        int sLeft = (obstacle1.getRight()-obstacle1.getLeft()) < (obstacle2.getRight() - obstacle2.getLeft()) ? obstacle1.getLeft() : obstacle2.getLeft();
        int sRight = (obstacle1.getRight()-obstacle1.getLeft()) < (obstacle2.getRight() - obstacle2.getLeft()) ? obstacle1.getRight() : obstacle2.getRight();
        int sTop = (obstacle1.getBottom() - obstacle1.getTop()) < (obstacle2.getBottom() - obstacle2.getTop()) ? obstacle1.getTop() : obstacle2.getTop();
        int sBottom = (obstacle1.getBottom() - obstacle1.getTop()) < (obstacle2.getBottom() - obstacle2.getTop()) ? obstacle1.getBottom() : obstacle2.getBottom();

        //assign who is the bigger side
        int bLeft = (obstacle1.getRight() - obstacle1.getLeft()) >= (obstacle2.getRight() - obstacle2.getLeft()) ? obstacle1.getLeft() : obstacle2.getLeft();
        int bRight = (obstacle1.getRight() - obstacle1.getLeft()) >= (obstacle2.getRight() - obstacle2.getLeft()) ? obstacle1.getRight() : obstacle2.getRight();
        int bTop = (obstacle1.getBottom() - obstacle1.getTop()) >= (obstacle2.getBottom() - obstacle2.getTop()) ? obstacle1.getTop() : obstacle2.getTop();
        int bBottom = (obstacle1.getBottom() - obstacle1.getTop()) >= (obstacle2.getBottom() - obstacle2.getTop()) ? obstacle1.getBottom() : obstacle2.getBottom();

        bLeft = bLeft - marginOfObstacle;
        bTop = bTop - marginOfObstacle;
        bRight = bRight + marginOfObstacle;
        bBottom = bBottom + marginOfObstacle;

        //Check if topLeft-Corner of smaller is in bigger
        if(sLeft >= bLeft && sLeft <= bRight) {
            if(sTop >= bTop && sTop <= bBottom){
                return true;
            }
        }

        //Check if topRight-Corner of smaller is in bigger
        if(sRight >= bLeft && sRight <= bRight) {
            if(sTop >= bTop && sTop <= bBottom){
                return true;
            }
        }

        //Check if bottomLeft-Corner of smaller is in bigger
        if(sLeft >= bLeft && sLeft <= bRight) {
            if(sBottom >= bTop && sBottom <= bBottom){
                return true;
            }
        }

        //Check if bottomRight-Corner of smaller is in bigger
        if(sRight >= bLeft && sRight <= bRight) {
            if(sBottom >= bTop && sBottom <= bBottom){
                return true;
            }
        }

        //smaller is not in bigger
        return false;
    }


}
