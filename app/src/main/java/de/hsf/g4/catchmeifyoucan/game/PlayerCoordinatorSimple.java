package de.hsf.g4.catchmeifyoucan.game;

import android.graphics.Point;
import android.util.Log;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.Player;

import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTED_START_X;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTED_START_Y;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTER_START_X;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTER_START_Y;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PLAYER_MAX_SPEED;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PLAYER_SIZE;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_WALL_LENGTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_X;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_Y;

public class PlayerCoordinatorSimple {

    String TAG = "PlayerCoordinatorSimple";

    private ArrayList<AtomicObstacle> obstaclesList;
    private ArrayList<PlayerFigure> playerFigures = new ArrayList<>();

    public ArrayList<PlayerFigure> getPlayerFigures() {
        return playerFigures;
    }

    public  PlayerCoordinatorSimple(ArrayList<Integer> playersID, ArrayList<Player.PlayerRole> playerRoles, ArrayList<AtomicObstacle> obstaclesList) {
        this.obstaclesList = obstaclesList;

        for(int i = 0; i < playersID.size(); i++) {
            playerFigures.add(new PlayerFigure(playersID.get(i), playerRoles.get(i)));

            if(playerFigures.get(i).getPlayerRole() == Player.PlayerRole.HUNTED){
                playerFigures.get(i).setXCoord(HUNTED_START_X);
                playerFigures.get(i).setYCoord(HUNTED_START_Y);
            } else {
                playerFigures.get(i).setXCoord(HUNTER_START_X);
                playerFigures.get(i).setYCoord(HUNTER_START_Y);
            }
        }
    }

    public void setNewPosition(ArrayList<GyroXYValues> gyroXYValues) {
        for(int playerCounter = 0; playerCounter < playerFigures.size(); playerCounter++){
            for(int gyroCounter = 0; gyroCounter < gyroXYValues.size(); gyroCounter++) {
                if(gyroXYValues.get(gyroCounter).getPlayerID() == playerFigures.get(playerCounter).getPlayerID()) {
                    movePlayerFigures(gyroXYValues.get(gyroCounter), playerFigures.get(playerCounter));
                }
            }

            if(playerFigures.size() > gyroXYValues.size()) {
                for(int i = 0; i < playerFigures.size() - gyroXYValues.size(); i++){
                    movePlayerFigures(new GyroXYValues(0,0,-1), playerFigures.get(playerCounter));
                }
            }
        }
    }

    private void movePlayerFigures(GyroXYValues gyroXYValues, PlayerFigure playerFigure){
        Point p3 = calcGyroP3(new Point(0, gyroXYValues.getY()) , new Point(gyroXYValues.getX(), 0));
        //Check if movement is over PLAYER_MAX_SPEED (|P3| > PLAYER_MAX_SPEED) else set to PLAYER_MAX_SPEED
        p3 = calcMovementPoint(new Point(playerFigure.getXCoord(),playerFigure.getYCoord()), p3);

        boolean playerIsCrashed = false;

        //if player is HUNTED, check if HUNTER got him
        if(playerFigure.getPlayerRole() == Player.PlayerRole.HUNTED) {
            for(int i = 0; i < playerFigures.size() && !playerIsCrashed; i++){
                if(playerFigures.get(i).getPlayerRole() == Player.PlayerRole.HUNTER &&
                        Crasher.isCrashedPlayerPlayer(new Point(playerFigures.get(i).getXCoord(),playerFigures.get(i).getYCoord()), p3)){
                    playerIsCrashed = true;
                }
            }
            if(playerIsCrashed) {
                //set new position
                playerFigure.setXCoord(PRISON_X + PRISON_WALL_LENGTH / 2 - PLAYER_SIZE / 2);
                playerFigure.setYCoord(PRISON_Y + PRISON_WALL_LENGTH / 2 - PLAYER_SIZE / 2);
            }
        }

        if(!playerIsCrashed) {
            //check if position is valid
            playerIsCrashed = false;
            for (int i = 0; i < obstaclesList.size() && !playerIsCrashed; i++) {
                if (Crasher.isCrashedObstaclePlayer(obstaclesList.get(i), p3)) {
                    playerIsCrashed = true;
                }
            }
            if (!playerIsCrashed) {
                //set new position
                playerFigure.setXCoord(p3.x);
                playerFigure.setYCoord(p3.y);
            } else {
                playerIsCrashed = false;
                for (int j = 0; j < obstaclesList.size() && !playerIsCrashed; j++) {
                    if (Crasher.isCrashedObstaclePlayer(obstaclesList.get(j), new Point(p3.x, playerFigure.getYCoord()))) {
                        playerIsCrashed = true;
                    }
                }
                if (!playerIsCrashed) {
                    //set new position
                    playerFigure.setXCoord(p3.x);
                    playerFigure.setYCoord(playerFigure.getYCoord());
                } else {
                    playerIsCrashed = false;
                    for (int k = 0; k < obstaclesList.size() && !playerIsCrashed; k++) {
                        if (Crasher.isCrashedObstaclePlayer(obstaclesList.get(k), new Point(playerFigure.getXCoord(), p3.y))) {
                            playerIsCrashed = true;
                        }
                    }
                    if (!playerIsCrashed) {
                        //set new position
                        playerFigure.setXCoord(playerFigure.getXCoord());
                        playerFigure.setYCoord(p3.y);
                    }
                }
            }
        }

    }

    /**
     * Check if movement is over PLAYER_MAX_SPEED (|P3| > PLAYER_MAX_SPEED) else set to PLAYER_MAX_SPEED
     * @param playerPosition current player position
     * @param directionVector movement direction vector
     * @return movementPoint Point with new Position
     */
    private Point calcMovementPoint(Point playerPosition, Point directionVector) {
        /* formula
         * P3 = P1 + a * DV
         */
        Point movementPoint;
        // direction vector length
        double dvLength = Math.sqrt(Math.pow(directionVector.x,2) + Math.pow(directionVector.y,2));
        //check length of directionVector
        if(dvLength > PLAYER_MAX_SPEED){
            /*
             * direction vector length = 100%
             * PLAYER_MAX_SPEED = a%
             *
             * a = ((100% / direction vector length) * PLAYER_MAX_SPEED) / 100
             */
            double a = ((100 / dvLength) * PLAYER_MAX_SPEED) / 100;

            movementPoint = new Point(
                                (int)(playerPosition.x + a * directionVector.x),
                                (int)(playerPosition.y + a * directionVector.y));
        }
        else {
            movementPoint = new Point(playerPosition.x + directionVector.x, playerPosition.y + directionVector.y);
        }
        return movementPoint;
    }

    /**
     * Calculates the point of movement where the x and y values are pointing to.
     *     /|\ y-axis
     *      |
     *   P1 X--.._
     *      |     --.._
     *      |          --.._  P3                a = alpha
     *      |  a      __...--X.._
     *      |__...---            --.._
     *      +-------------------------X----> x-axis
     *                                 P2
     * @param p10Y Point1
     * @param p2X0 Point2
     * @return P3 a Point between P1 and P2
     */
    private Point calcGyroP3(Point p10Y, Point p2X0) {
        /* formula
         * P3 = P1 + a * DV
         */

        /* a
         * depends on how much X and Y have contributed
         */
        double a = (100 / (double)(Math.abs(p10Y.y) + Math.abs(p2X0.x)) * Math.abs(p2X0.x) / 100);

        //direction vector (DV)
        int dx = p2X0.x - p10Y.x;
        int dy = p2X0.y - p10Y.y;


        int p3x = (int)(p10Y.x + a * dx);
        int p3y = (int)(p10Y.y + a * dy);

        return new Point(p3x, p3y);
    }
}
