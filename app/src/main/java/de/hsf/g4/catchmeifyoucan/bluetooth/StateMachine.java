package de.hsf.g4.catchmeifyoucan.bluetooth;

import android.os.Handler;

public class StateMachine extends Handler {
    private static final String TAG = "hsflStateMachine";

    @Override
    public void handleMessage(android.os.Message message) {
        theBrain(message);
    }           //##0b

    /**
     * virtual method, must be overwritten in subclass
     * @param message
     */
    void theBrain(android.os.Message message){
    }

    protected void setTimer(int messageType, long durationMs){
        android.os.Message msg = new android.os.Message();
        msg.what = messageType;
        msg.arg1 = 0;
        msg.arg2 = 0;
        sendMessageDelayed(msg, durationMs);                                                //##4a
    }

    protected void stopTimer(int messageType){
        removeMessages(messageType);                                                        //##4b
    }

    public void sendSmMessage(int messageType, int arg1, int arg2, Object obj){
        // PrintData d = new PrintData(3);                                                   //##3a
        android.os.Message msg = new android.os.Message();                                   //##0a
        msg.what = messageType;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        if ( obj != null ) {
            msg.obj = obj;
        }
        this.sendMessage(msg);
    }

}