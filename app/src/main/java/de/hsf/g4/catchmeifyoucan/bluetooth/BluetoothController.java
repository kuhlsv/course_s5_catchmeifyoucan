package de.hsf.g4.catchmeifyoucan.bluetooth;

import android.app.Activity;
import android.support.v4.app.Fragment;


import java.util.ArrayList;
import java.util.UUID;

import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.game.AtomicObstacle;
import de.hsf.g4.catchmeifyoucan.game.GyroXYValues;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;

public interface BluetoothController {
    public void initAsClient(Activity activity, String btServerAddress, UUID myUUID, String service, boolean toast);
    public void initAsServer(Activity activity, UUID myUUID, String service, boolean toast);
    public void setListener(Fragment frag);
    public void start();
    public void send(String str);
    public void sendPlayer(Player player);
    public void sendPlayerFigures(ArrayList<PlayerFigure> figs);
    public void sendObstacles(ArrayList<AtomicObstacle> obstacles);
    public void sendGyroValues(GyroXYValues gyroVals);
    public void stop();
    public String getRole();

}

