package de.hsf.g4.catchmeifyoucan.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.sensors.Sensors;

import static de.hsf.g4.catchmeifyoucan.game.GameProperties.ASPECT_RATION_HEIGHT;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.ASPECT_RATION_WIDTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.BACKGROUND_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTED_PLAYER_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTED_PLAYER_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTER_PLAYER_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.HUNTER_PLAYER_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.ILLUMINANCE_LIMIT;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_HEIGHT;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_WIDTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_CORNER_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_CORNER_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_CROSS_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_CROSS_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_HORIZONTAL_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_HORIZONTAL_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_VERTICAL_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_VERTICAL_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OWN_PLAYER_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OWN_PLAYER_COLOR_DARK;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PLAYER_SIZE;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_COLOR;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_COLOR_DARK;

public class Display extends View {

    private static final String TAG ="Display";

    private ArrayList<AtomicObstacle> obstacleList;

    public void setObstacleList(ArrayList<AtomicObstacle> obstacleList) {
        this.obstacleList = obstacleList;
    }

    private ArrayList<PlayerFigure> playerFiguresList = new ArrayList<>();

    public void setPlayerFiguresList(ArrayList<PlayerFigure> playerFiguresList) {
        this.playerFiguresList = playerFiguresList;
    }

    int canvasWidth;
    int canvasHeight;

    int mapWidth;
    int mapHeight;

    int mapMarginSide;
    int mapMarginTopBottom;

    float scaleFactorX = 1;
    float scaleFactorY = 1;

    int ownPlayerID;
    
    private Sensors lightSensor;
    private boolean exceededIlluminanceLimit = false;

    public Display(Context context, ArrayList<AtomicObstacle> obstacleList, Integer ownPlayerID, Sensors sensor) {
        super(context);
        this.obstacleList = obstacleList;
        this.ownPlayerID = ownPlayerID;
        this.lightSensor = sensor;
    }

    public Display(Context context, Integer ownPlayerID, Sensors sensor) {
        super(context);
        this.obstacleList = new ArrayList<>();
        this.ownPlayerID = ownPlayerID;
        this.lightSensor = sensor;
    }

    public Display(Context context) {
        super(context);
    }

    public Display(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Display(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onDraw(Canvas canvas) {
        // check if illuminance of environment is exceeding the limit value
        if (this.lightSensor.getIlluminance() > ILLUMINANCE_LIMIT){
            exceededIlluminanceLimit = true;
        }else {
            exceededIlluminanceLimit = false;
        }

        if(exceededIlluminanceLimit) {
            canvas.drawColor(BACKGROUND_COLOR);
        }else {
            canvas.drawColor(GameProperties.BACKGROUND_COLOR_DARK);
        }
        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();

        //orientation on width
        int tempMapHeight1 = canvasWidth / ASPECT_RATION_WIDTH * ASPECT_RATION_HEIGHT;
        int tempMapWidth1 = canvasWidth;

        //orientation on height
        int tempMapHeight2 = canvasHeight;
        int tempMapWidth2 = canvasHeight / ASPECT_RATION_HEIGHT * ASPECT_RATION_WIDTH;

        if(tempMapHeight1 <= tempMapHeight2) {
            mapHeight = tempMapHeight1;
            mapWidth = tempMapWidth1;
        } else {
            mapHeight = tempMapHeight2;
            mapWidth = tempMapWidth2;
        }

        scaleFactorX = (float)mapWidth / (float)MAP_WIDTH;
        scaleFactorY = (float)mapHeight / (float)MAP_HEIGHT;

        getMap().draw(canvas);

        for(AtomicObstacle obstacle : obstacleList){
            getObstacle(obstacle).draw(canvas);
        }

        for(PlayerFigure player : playerFiguresList){
            getPlayerFigure(player).draw(canvas);
        }
    }

    private ShapeDrawable getMap() {
        mapMarginSide = (canvasWidth - mapWidth)/2;
        mapMarginTopBottom = (canvasHeight - mapHeight)/2;
        ShapeDrawable map = new ShapeDrawable(new RectShape());
        if(exceededIlluminanceLimit) {
            map.getPaint().setColor(MAP_COLOR);
        }else {
            map.getPaint().setColor(GameProperties.MAP_COLOR_DARK);
        }

        map.setBounds(mapMarginSide,
                mapMarginTopBottom,
                mapMarginSide + mapWidth,
                mapMarginTopBottom + mapHeight);
        return map;
    }

    private ShapeDrawable getObstacle(AtomicObstacle obstacleData){
        ShapeDrawable obstacle = new ShapeDrawable(new RectShape());
        // if limit is exceeded -> LIGHT THEME , else DARK THEME
        if(exceededIlluminanceLimit) {
            switch (obstacleData.getObstaclePartOf()) {
                case WallHor:
                    obstacle.getPaint().setColor(OBSTACLE_WALL_HORIZONTAL_COLOR);
                    break;
                case WallVer:
                    obstacle.getPaint().setColor(OBSTACLE_WALL_VERTICAL_COLOR);
                    break;
                case Cross:
                    obstacle.getPaint().setColor(OBSTACLE_CROSS_COLOR);
                    break;
                case Corner:
                    obstacle.getPaint().setColor(OBSTACLE_CORNER_COLOR);
                    break;
                case Prison:
                    obstacle.getPaint().setColor(PRISON_COLOR);
                    break;
                default:
                    obstacle.getPaint().setColor(Color.rgb(255, 94, 198));
            }
        }else{
            switch (obstacleData.getObstaclePartOf()) {
                case WallHor:
                    obstacle.getPaint().setColor(OBSTACLE_WALL_HORIZONTAL_COLOR_DARK);
                    break;
                case WallVer:
                    obstacle.getPaint().setColor(OBSTACLE_WALL_VERTICAL_COLOR_DARK);
                    break;
                case Cross:
                    obstacle.getPaint().setColor(OBSTACLE_CROSS_COLOR_DARK);
                    break;
                case Corner:
                    obstacle.getPaint().setColor(OBSTACLE_CORNER_COLOR_DARK);
                    break;
                case Prison:
                    obstacle.getPaint().setColor(PRISON_COLOR_DARK);
                    break;
                default:
                    obstacle.getPaint().setColor(Color.rgb(255, 94, 198));
            }
        }
        obstacle.setBounds((int)(obstacleData.getLeft() * scaleFactorX + mapMarginSide),
                (int)(obstacleData.getTop() * scaleFactorY + mapMarginTopBottom),
                (int)(obstacleData.getRight() * scaleFactorX + mapMarginSide),
                (int)(obstacleData.getBottom() * scaleFactorY + mapMarginTopBottom));
        return obstacle;
    }

    private ShapeDrawable getPlayerFigure(PlayerFigure playerFigure) {
        ShapeDrawable player = new ShapeDrawable(new OvalShape());
        // if limit is exceeded -> LIGHT THEME , else DARK THEME
        if(exceededIlluminanceLimit) {
            if (playerFigure.getPlayerID() == ownPlayerID) {
                player.getPaint().setColor(OWN_PLAYER_COLOR);
            } else if (playerFigure.getPlayerRole() == Player.PlayerRole.HUNTED) {
                player.getPaint().setColor(HUNTED_PLAYER_COLOR);
            } else if (playerFigure.getPlayerRole() == Player.PlayerRole.HUNTER) {
                player.getPaint().setColor(HUNTER_PLAYER_COLOR);
            }
        }else {
            if (playerFigure.getPlayerID() == ownPlayerID) {
                player.getPaint().setColor(OWN_PLAYER_COLOR_DARK);
            } else if (playerFigure.getPlayerRole() == Player.PlayerRole.HUNTED) {
                player.getPaint().setColor(HUNTED_PLAYER_COLOR_DARK);
            } else if (playerFigure.getPlayerRole() == Player.PlayerRole.HUNTER) {
                player.getPaint().setColor(HUNTER_PLAYER_COLOR_DARK);
            }
        }
        player.setBounds((int)(playerFigure.getXCoord() * scaleFactorX + mapMarginSide),
                (int)(playerFigure.getYCoord() * scaleFactorY + mapMarginTopBottom),
                (int)((playerFigure.getXCoord() + PLAYER_SIZE) * scaleFactorX + mapMarginSide),
                (int)((playerFigure.getYCoord() + PLAYER_SIZE) * scaleFactorY + mapMarginTopBottom));
        return player;
    }
}
