package de.hsf.g4.catchmeifyoucan.bluetooth;

import android.app.Activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.game.AtomicObstacle;
import de.hsf.g4.catchmeifyoucan.game.GyroXYValues;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;

public class Controller extends StateMachine implements BluetoothController {

    private static final String TAG = "hsflController";
    private OnBluetoothListener mUiListener = null;
    private Activity mActivity = null;

    private AcceptThread mAcceptThread;
    private ConnectedThread mConnectedThread;
    private ConnectThread mConnectThread;

    private BluetoothDevice mDevice;
    private String mBTAddress = "";
    private String mRole = "not init";
    private Boolean mToast;

    private UUID mUUID;
    private String mServiceName = "";
    private DAO dao = DAO.getInstance();

    BluetoothAdapter mBluetoothAdapter;

    protected static final int REQUEST_ENABLE_BT = 1;
    protected static final String ROLE_SERVER = "Server";
    protected static final String ROLE_CLIENT = "Client";

    public enum SmMessage {
        UI_START, UI_STOP, UI_SEND,UI_SEND_PLAYER,
        UI_SEND_PLAYER_FIGURE, UI_SEND_OBSTACLES,
        UI_SEND_GYROVALS,                               // from UI
        CO_INIT, TIMER_CLIENT_RECONNECT,                // to Controller
        ST_MANAGE_CONNECTED_SOCKET, ST_DEBUG,           // from SocketThread (Accept- or ConnectThread)
        CT_RECEIVED, CT_CONNECTION_CLOSED, CT_DEBUG     // from ConnectedThread
    }

    private enum State {
        START, IDLE, WAIT_FOR_CONNECT, CONNECTED, NO_BLUETOOTH, NO_INIT
    }

    public State state = State.START;        // the state variable

    public static SmMessage[] messageIndex = SmMessage.values();

    private Controller() {
        Log.d(TAG, "Controller()");
    }

    private static Controller instance = null;

    public static Controller getInstance(){
        if(instance == null){
            instance = new Controller();
        }
        return instance;
    }

    @Override
    public void initAsClient(Activity activity, String btServerAddress, UUID myUUID, String service, boolean toast) {
        Log.d(TAG, "initAsClient()");

        mRole = ROLE_CLIENT;
        init(activity, btServerAddress, myUUID, service, toast);
    }

    @Override
    public void initAsServer(Activity activity, UUID myUUID, String service, boolean toast) {
        Log.d(TAG, "initAsServer()");

        mRole = ROLE_SERVER;
        init(activity, "nix", myUUID, service, toast);
    }

    @Override
    public void start() {
        sendSmMessage(Controller.SmMessage.UI_START.ordinal(), 0, 0, null);
    }

    @Override
    public void stop() {
        sendSmMessage(Controller.SmMessage.UI_STOP.ordinal(), 0, 0, null);
    }

    @Override
    public void send(String str) {
        Log.d(TAG, "About to send: " + str);
        sendSmMessage(Controller.SmMessage.UI_SEND.ordinal(), 0, 0, str);
    }

    @Override
    public void sendPlayer(Player player){
        Log.d("hsflPlayer", "Send Player");
        sendSmMessage(SmMessage.UI_SEND_PLAYER.ordinal(),0,0,player);
    }


    @Override
    public void sendPlayerFigures(ArrayList<PlayerFigure> figs){
        sendSmMessage(SmMessage.UI_SEND_PLAYER_FIGURE.ordinal(), 0,0,figs);
    }

    @Override
    public void sendObstacles(ArrayList<AtomicObstacle> obstacles){
        sendSmMessage(SmMessage.UI_SEND_OBSTACLES.ordinal(),0,0, obstacles);
    }

    @Override
    public void sendGyroValues(GyroXYValues gyroVals){
        sendSmMessage(SmMessage.UI_SEND_GYROVALS.ordinal(), 0 ,0, gyroVals);
    }

    @Override
    public String getRole() { return mRole; };

    // setState
    private void setState(State state, boolean isActive) {
        this.state = state;
        mUiListener.onStateChanged(state.toString());
        mUiListener.onActiveChanged(isActive);
        if (mToast)
            Toast.makeText( mActivity, "Bluetooth " + mRole + " State:\n" + state.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setListener (Fragment frag) {
        Log.d(TAG, "setListener()");

        try {
            mUiListener = (OnBluetoothListener) frag;
        } catch (ClassCastException e) {
            throw new ClassCastException(frag.toString()
                    + " must implement OnFragmentInteractionListener !!!!!!! ");
        }
    }

    // Alert-Dialog
    private void alert(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(text)
                .setCancelable(false)
                .setNegativeButton("it's understood", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void init(Activity a, String address, UUID myUUID, String service, boolean toast) {
        Log.d(TAG, "init()");

        mActivity = a;
        mBTAddress = address;
        mUUID = myUUID;
        mServiceName = service;
        mToast = toast;

        // send message for start transition
        sendSmMessage(SmMessage.CO_INIT.ordinal(), 0, 0, null);
    }


    /**
     * the statemachine
     *
     *   call it only via sendSmMessage()
     *
     * @param message
     */
    @Override
    void theBrain(android.os.Message message){
        SmMessage inputSmMessage = messageIndex[message.what];

        // erstmal ohne SM-Logging die Debug-Meldungen der Threads verarbeiten
        if ( inputSmMessage == SmMessage.ST_DEBUG ) {
            if (mRole.equals(ROLE_CLIENT)) {
                Log.d("ConnectThread: ", (String) message.obj);
            } else
                Log.d("AcceptThread: ", (String) message.obj);
            return;
        }

        if ( inputSmMessage == SmMessage.CT_DEBUG ) {
            Log.d("ConnectedThread: ", (String) message.obj);
            return;
        }

        // jetzt gehts erst richtig los
        Log.i(TAG, "SM: state: " + state + ", input message: " +
                inputSmMessage.toString() + ", arg1: " +
                message.arg1 + ", arg2: " + message.arg2);
        if (message.obj != null){
            Log.i(TAG, "SM: data: " + message.obj.toString());
        }

        // der Rest
        switch ( state ) {
            case START:
                switch (inputSmMessage) {
                    case CO_INIT:
                        Log.v(TAG, "in Init");

                        if (mRole.equals("")) {
                            Log.d(TAG, "Error: initAsClient() oder initAsServer() wurde nicht aufgerufen !!!");
                            if (mToast) {
                                alert("Error: initAsClient() oder initAsServer() wurde nicht aufgerufen !!!");
                            }

                            setState(State.NO_INIT, false);
                            break; // case SmMessage CO_INIT
                        }

                        Log.d(TAG, "Init Bluetooth");
                        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter == null) {
                            Log.d(TAG, "Error: Device does not support Bluetooth !!!");

                            if (mToast) {
                                alert("Error: Device does not support Bluetooth !!!");
                            }

                            setState(State.NO_BLUETOOTH, false);
                            break; // case SmMessage CO_INIT
                        }

                        setState(State.IDLE, false);
                        break; // case SmMessage CO_INIT
                    default: // inputSmMessage, state START
                        Log.v(TAG, "SM: not a valid input in this state: " + state.toString());
                        break;
                }
                break; // state START
            case IDLE:
                switch (inputSmMessage) {
                    case UI_START:

                        if (!mBluetoothAdapter.isEnabled() ) {
                            Log.d(TAG, "Try to enable Bluetooth.");
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            mActivity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                            // returnvalue koennte hier evaluiert werden, s. Foliensatz

                            setState(State.IDLE, false);
                            break; // case SMMessage UI_START
                        }

                        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
                        Log.d(TAG, "paired devices:");
                        if (pairedDevices.size() > 0) {
                            // Loop through paired devices
                            for (BluetoothDevice device : pairedDevices) {
                                Log.d(TAG, "   " + device.getName() + "  " + device.getAddress());
                            }
                        }
                        mBluetoothAdapter.cancelDiscovery();
                        if (mRole.equals(ROLE_CLIENT)) {
                            Log.d(TAG, "instance ConnectThread");

                            mDevice = mBluetoothAdapter.getDefaultAdapter().getRemoteDevice(mBTAddress);

                            mConnectThread = new ConnectThread(mDevice, this, mUUID);
                            mConnectThread.start();

                            //Timer 5 sec
                            setTimer(SmMessage.TIMER_CLIENT_RECONNECT.ordinal(), 5000);

                        } else {
                            Log.d(TAG, "instance AcceptThread");
                            mAcceptThread = new AcceptThread(mBluetoothAdapter, this, mUUID, mServiceName);
                            mAcceptThread.start();
                        }

                        setState(State.WAIT_FOR_CONNECT, true);
                        break; // case SMMessage UI_START, state IDLE
                    default: // inputSmMessage, state IDLE
                        Log.v(TAG, "SM: not a valid input in this state: " + state.toString());
                        break;
                }
                break; // state IDLE

            case WAIT_FOR_CONNECT:
                switch (inputSmMessage) {
                    case UI_STOP:
                        if (mRole.equals(ROLE_CLIENT)) {
                            mConnectThread.cancel();
                        } else {
                            mAcceptThread.cancel();
                        }
                        setState(State.IDLE, false);
                        break; // case SMMessage UI_STOP

                    case ST_MANAGE_CONNECTED_SOCKET:
                        if (mRole.equals(ROLE_CLIENT)) {
                            stopTimer(SmMessage.TIMER_CLIENT_RECONNECT.ordinal());
                        } else {
                            mAcceptThread.cancel();
                        }
                        mConnectedThread = new ConnectedThread((BluetoothSocket)message.obj, this);
                        mConnectedThread.start();

                        setState(State.CONNECTED, true);
                        break; // case SMMessage ST_MANAGE_CONNECTED_SOCKET

                    case TIMER_CLIENT_RECONNECT:
                        mConnectThread.cancel();
                        mConnectThread = new ConnectThread(mDevice, this, mUUID);
                        mConnectThread.start();
                        Log.v(TAG, "inside timer client reconnect");
                        setTimer(SmMessage.TIMER_CLIENT_RECONNECT.ordinal(), 5000);
                        break; // case SMMessage TIMER_CLIENT_RECONNECT

                    default: // inputSmMessage, state WAIT_FOR_CONNECT
                        Log.v(TAG, "SM: not a valid input in this state: " + state.toString());
                        break;
                }
                break; // state WAIT_FOR_CONNECT

            case CONNECTED:
                switch (inputSmMessage) {
                    case UI_SEND:
                        mConnectedThread.write(("0" + (String) message.obj).getBytes());
                        Log.d(TAG, "writing " +  message.obj);
                        break; // case SMMessage UI_SEND
                    case UI_SEND_PLAYER:
                        try{
                            Log.d(TAG,"Serialized Player " + objectToBase64((Player)message.obj));
                            mConnectedThread.write(("1" + objectToBase64((Player)message.obj)).getBytes());
                        }catch(IOException e){
                            Log.d(TAG, "couldnt send Player");
                        }
                        break;
                    case UI_SEND_PLAYER_FIGURE:
                        try{
                            Log.d(TAG,"Serialized PlayerFigure " + objectToBase64((ArrayList<PlayerFigure>)message.obj));
                            mConnectedThread.write(("2" + objectToBase64((ArrayList<PlayerFigure>)message.obj)).getBytes());
                        }catch(IOException e){
                            Log.d(TAG, "couldnt send PlayerFigure");
                        }
                        break;
                    case UI_SEND_OBSTACLES:
                        try{
                            mConnectedThread.write(("3" + objectToBase64((ArrayList<AtomicObstacle>)message.obj)).getBytes());
                        }catch(IOException e){
                            Log.d(TAG, "couldnt send obstacles");
                        }
                    case UI_SEND_GYROVALS:
                        try{
                            Log.d(TAG,"Serialized Gyrovals ");
                            mConnectedThread.write(("4" + objectToBase64((GyroXYValues)message.obj)).getBytes());
                        }catch(Exception e) {
                            Log.d(TAG, "couldnt send gyroVals");
                        }
                    case CT_RECEIVED:
                        try{
                            String str = new String((byte[]) message.obj, 0, message.arg1);
                            Log.d(TAG, "Message: " + str);

                            char prefix = str.charAt(0);
                            switch(prefix){
                                case '0':
                                    mUiListener.onReceived( str.substring(1) );
                                    break;
                                case '1':
                                    try{
                                        Player player = (Player)base64ToObject(str.substring(1));
                                        if(player != null){
                                            ArrayList<Player> players = new ArrayList<>();
                                            players.add(dao.getSelf());
                                            players.add(player);
                                            dao.setPlayers(players);
                                            Log.d("hsflListSize","Players set: " + dao.getPlayers().size());
                                        }else{
                                            Log.d("hsflListSize", "Player is null");
                                        }
                                    }catch(Exception e) {
                                        Log.d(TAG, "Couldnt read players: " + e.getMessage());
                                    }
                                    break;
                                case '2':
                                    try{
                                        ArrayList<PlayerFigure> playerFigures = (ArrayList<PlayerFigure>)base64ToObject(str.substring(1));
                                        this.dao.setPlayerFigures(playerFigures);
                                        Log.d(TAG,"Set player Figures");
                                    }catch(Exception e) {
                                        Log.d(TAG, "couldnt read playerFigures: " + e.getMessage());
                                    }
                                    break;
                                case '3':
                                    try {
                                        ArrayList<AtomicObstacle> obstacles = (ArrayList<AtomicObstacle>)base64ToObject(str.substring(1));
                                        this.dao.setObstacles(obstacles);
                                        Log.d(TAG,"Obstacles set");
                                    }catch(Exception e) {
                                        Log.d(TAG, "Couldnt read obstacles");
                                    }
                                    break;
                                case '4':
                                    try{
                                        GyroXYValues gyroVals = (GyroXYValues)base64ToObject(str.substring(1));
                                        ArrayList<GyroXYValues> gyroList = new ArrayList<>();
                                        gyroList.add(gyroVals);
                                        this.dao.setGyroValues(gyroList);
                                        Log.d(TAG,"Set Gyro Values");
                                    }catch(Exception ex4){
                                        Log.d(TAG,"Couldnt read gyroValues");
                                    }
                                    break;
                            }
                            //set opponent player in dao
                        }catch(Exception failed){
                            Log.d(TAG, failed.getMessage());
                        }
                        break;
                    case CT_CONNECTION_CLOSED:
                        if (mRole.equals(ROLE_CLIENT)) {
                            mConnectedThread.cancel();
                            mConnectThread.cancel();

                            // try reconnect
                            mConnectThread = new ConnectThread(mDevice, this, mUUID);
                            mConnectThread.start();
                            setTimer(SmMessage.TIMER_CLIENT_RECONNECT.ordinal(), 5000);
                        } else {
                            mConnectedThread.cancel();
                            mAcceptThread.cancel();
                            mAcceptThread = new AcceptThread(mBluetoothAdapter, this, mUUID, mServiceName);
                            mAcceptThread.start();
                        }

                        setState(State.WAIT_FOR_CONNECT, true);
                        break; // case SMMessage CT_CONNECTION_CLOSED
                    case UI_STOP:
                        if (mRole.equals(ROLE_CLIENT)) {
                            stopTimer(SmMessage.TIMER_CLIENT_RECONNECT.ordinal());
                        }
                        mConnectedThread.cancel();

                        setState(State.IDLE, false);
                        break; // case SMMessage UI_STOP

                    default: // inputSmMessage, state CONNECTED
                        Log.v(TAG, "SM: not a valid input in this state: " + state.toString());
                        break;
                }
                break; // state CONNECTED
            case NO_BLUETOOTH:      // End States
                break; // state NO_BLUETOOTH
            case NO_INIT:
                switch (inputSmMessage) {
                    default: // inputSmMessage, state NO_INIT
                        Log.v(TAG, "SM: not a valid input in this state: " + state.toString());
                        break;
                }
                break; // state NO_INIT
        }

        Log.i(TAG, "SM: new State: " + state);
    }

    public Object base64ToObject(String s) throws IOException, ClassNotFoundException {
        byte [] data = Base64.decode(s, Base64.DEFAULT);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    public String objectToBase64( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }
}