package de.hsf.g4.catchmeifyoucan;

import java.io.Serializable;
import java.util.Random;

public class Player implements Serializable {

    private static final String TAG = Player.class.getSimpleName();
    private static final int minID = 1000;
    private static final int maxID = 1000000;

    public enum PlayerRole {
        HUNTER, HUNTED
    }

    private String name;
    private int playerID;
    private Boolean nameIsSet;
    private Boolean isHost;
    private int totalWins;
    private PlayerRole playerRole;
    private boolean isReady;
    // MAC (BT) or IP (WLan)
    private String connectionID;

    public Player(int playerID){
        this.playerID = playerID;
        this.nameIsSet = false;
        this.isHost = false;
        this.name = "Player" + playerID;
        this.totalWins = 0;
        this.isReady = false;
    }

    public PlayerRole getPlayerRole() {
        return playerRole;
    }

    public void setPlayerRole(PlayerRole playerRole) {
        this.playerRole = playerRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalWins() {
        return totalWins;
    }

    public void setTotalWins(int totalWins) {
        this.totalWins = totalWins;
    }

    public Boolean getNameIsSet() {
        return nameIsSet;
    }

    public void setNameIsSet(Boolean nameIsSet) {
        this.nameIsSet = nameIsSet;
    }

    public Boolean getHost() {
        return isHost;
    }

    public void setHost(Boolean host) {
        isHost = host;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public String getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(String connectionID) {
        this.connectionID = connectionID;
    }

    public static int createID(Random r){
        // Exclusive max, inclusive min
        return r.nextInt(maxID - minID) + minID;
    }

    @Override
    public String toString()  {
        return getName() + " " + getPlayerRole() + " " + getTotalWins();
    }

}
