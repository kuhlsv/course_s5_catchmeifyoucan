package de.hsf.g4.catchmeifyoucan.game;

import java.io.Serializable;

public class GyroXYValues implements Serializable {

    private int x;
    private int y;
    private int playerID;

    public GyroXYValues(int x, int y, int playerID) {
        this.x = x;
        this.y = y;
        this.playerID = playerID;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }
}
