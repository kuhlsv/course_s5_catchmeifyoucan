package de.hsf.g4.catchmeifyoucan.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import de.hsf.g4.catchmeifyoucan.Player;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private final static String DATABASE_NAME = "CatchMeIfYouCan";
    private final static int DATABASE_VERSION = 1;
    // Tables
    private final static String TABLE_PLAYER= "CatchMeIfYouCan";
    private final static String COLUMN_PLAYER_ID = "ID";
    private final static String COLUMN_PLAYER_NAME = "Name";
    private final static String COLUMN_PLAYER_NAMESET= "NameIsSet";
    private final static String COLUMN_PLAYER_SCORE = "TotalScore";

    public DatabaseHelper(Context context)  {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "DatabaseHelper.onCreate ... ");
        // Script to create table.
        String script = "CREATE TABLE IF NOT EXISTS " + TABLE_PLAYER + "("
                + COLUMN_PLAYER_ID + " INTEGER PRIMARY KEY," + COLUMN_PLAYER_NAME + " TEXT," + COLUMN_PLAYER_NAMESET + " BOOLEAN DEFAULT 0,"
                + COLUMN_PLAYER_SCORE + " INTEGER" + ")";
        // Execute script.
        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "DatabaseHelper.onUpgrade ... ");
        // Drop table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYER);
        // Recreate
        onCreate(db);
    }

    public void dropDatabase(SQLiteDatabase db) {
        Log.i(TAG, "DatabaseHelper.dropDB ... ");
        // Drop table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYER);
    }


    public void addPlayer(Player player) {
        Log.i(TAG, "DatabaseHelper.addUser ... " + player.toString());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYER_ID, player.getPlayerID());
        values.put(COLUMN_PLAYER_NAME, player.getName());
        values.put(COLUMN_PLAYER_NAMESET, player.getNameIsSet());
        values.put(COLUMN_PLAYER_SCORE, player.getTotalWins());
        // Inserting Row
        db.insert(TABLE_PLAYER, null, values);
        // Closing database connection
        db.close();
    }

    public Player getPlayer() {
        Log.i(TAG, "DatabaseHelper.getUser ... ");
        Cursor cursor = getPlayerCursor();
        Player user = null;
        if (cursor != null){
            cursor.moveToFirst();
            try{
                user = new Player(Integer.parseInt(cursor.getString(0)));
                user.setName(cursor.getString(1));
                user.setNameIsSet(!(cursor.getString(2) == "0"));
                user.setTotalWins(Integer.parseInt(cursor.getString(3)));
                //debug Log.e(TAG, user.toString());
            }catch(Exception e){
                Log.e(TAG, e.getMessage());
            }
        }
        // return player
        return user;
    }

    public Cursor getPlayerCursor() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PLAYER, new String[] { COLUMN_PLAYER_ID,
                        COLUMN_PLAYER_NAME, COLUMN_PLAYER_NAMESET, COLUMN_PLAYER_SCORE }, null ,
                null, null, null, null, null);
        return cursor;
    }

    public int updatePlayer(Player player) {
        Log.i(TAG, "DatabaseHelper.updateUser ... "  + player.toString());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYER_ID, player.getPlayerID());
        values.put(COLUMN_PLAYER_NAME, player.getName());
        values.put(COLUMN_PLAYER_NAMESET, player.getNameIsSet());
        values.put(COLUMN_PLAYER_SCORE, player.getTotalWins());
        // updating row
        return db.update(TABLE_PLAYER, values, COLUMN_PLAYER_ID + " = ?",
                new String[]{String.valueOf(player.getPlayerID())});
    }

    public void deletePlayer(Player player) {
        Log.i(TAG, "DatabaseHelper.updateUser ... " + player.getPlayerID());
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PLAYER, COLUMN_PLAYER_ID + " = ?",
                new String[] { String.valueOf(player.getPlayerID()) });
        db.close();
    }

}
