package de.hsf.g4.catchmeifyoucan.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ConnectThread extends Thread {

    private BluetoothSocket mSocket;
    private BluetoothDevice mDevice;
    private Controller mController;
    private UUID mUUID;

    public ConnectThread(BluetoothDevice device, Controller controller, UUID uuID) {

        mDevice = device;
        mController = controller;
        mUUID = uuID;

        BluetoothSocket tmp = null;
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = mDevice.createRfcommSocketToServiceRecord(mUUID);
        } catch (IOException e) {
            debugOut("ConnectThread(): IOException when trying to initialize Socket");
        }
        mSocket = tmp;
    }

    public void run() {
        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mSocket.connect();
            debugOut("ConnectThread(): AT_MANAGE_CONNECTED_SOCKET");
            mController.obtainMessage(Controller.SmMessage.ST_MANAGE_CONNECTED_SOCKET.ordinal(),
                    -1, -1, mSocket).sendToTarget();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                mSocket.close();
            } catch (IOException closeException) {
                debugOut("ConnectThread(): IOException when trying to close Socket");
            }
            return;
        }

        // Do work to manage the connection (in a separate thread)
//        debugOut("ConnectThread(): AT_MANAGE_CONNECTED_SOCKET");
//        mController.obtainMessage(Controller.SmMessage.AT_MANAGE_CONNECTED_SOCKET.ordinal(),
//                -1, -1, mSocket).sendToTarget();
    }

    public void cancel() {
        debugOut("cancel()");
        try {
            mSocket.close();
        } catch (IOException e) {
            debugOut("cancel(): IOException during closing socket !!!");
        }
    }

    private void debugOut(String str) {
        mController.obtainMessage(Controller.SmMessage.ST_DEBUG.ordinal(),
                -1, -1, str).sendToTarget();
    };
}