package de.hsf.g4.catchmeifyoucan.wlan;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.util.Base64;

public class WlanRequestBuilder {

    public WlanRequestBuilder() {

    }

    public String build(String type, Serializable content) {
        try {
            return type + ":" + objectToBase64(content).replaceAll("\n", "");
        } catch(java.io.IOException e) {
            return null;
        }
    }

    // util

    public Object base64ToObject(String s) throws IOException, ClassNotFoundException {
        byte [] data = Base64.decode(s, Base64.DEFAULT);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    public String objectToBase64(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }
}
