package de.hsf.g4.catchmeifyoucan.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.wlan.BroadcastService;
import de.hsf.g4.catchmeifyoucan.wlan.ConnectionChangedReceiver;
import de.hsf.g4.catchmeifyoucan.wlan.WlanClient;
import de.hsf.g4.catchmeifyoucan.wlan.WlanServer;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class OverviewFragment extends Fragment {

    private static final String TAG = OverviewFragment.class.getSimpleName();
    // List to show in view
    ArrayList<String> listItems = new ArrayList<>();
    // To compare and remove old items
    ArrayList<String> bufferList = new ArrayList<>();
    ArrayAdapter<String> listAdapter;
    // Time to update list
    private final Handler updateUIHandler = new Handler();
    private Runnable updateUIRunnable;
    private Timer t;
    private BroadcastReceiver hostReceiver;
    private ConnectionChangedReceiver wifiReceiver;
    private Intent i;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Player player = DAO.getInstance().getSelf();

        // Register Receiver
        // Wifi changed
        IntentFilter intentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        this.wifiReceiver = new ConnectionChangedReceiver();
        getActivity().registerReceiver(this.wifiReceiver, intentFilter);

        // Start receiver broadcast
        // Start and trigger the Wlan service to send broadcast for hosting
        Context context = getContext();
        i = new Intent(context, BroadcastService.class);
        // add data to the intent for the service
        i.putExtra("IP", player.getConnectionID());
        i.putExtra("Name", player.getName());
        i.putExtra("IsHost", "false");
        getActivity().getApplicationContext().startService(i);

        // Register broadcast receiver to get data when host is found
        IntentFilter filter = new IntentFilter();
        filter.addAction("BroadcastService.FoundHost");
        this.hostReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, intent.getAction());
                if(intent.getAction().equals("BroadcastService.FoundHost")){
                    String hostName = intent.getStringExtra("Name");
                    String hostIP = intent.getStringExtra("IP");
                    if(!hostName.equals("") && !hostIP.equals("")){
                        updateHostList(hostName, hostIP);
                    }
                }
            }
        };
        context.registerReceiver(this.hostReceiver, filter);

        // Check buffered list with item list to remove old hosts in interval
        updateUIRunnable = new Runnable() {
            public void run() {
                checkLists();
            }
        };
        //Declare the timer
        t = new Timer();
        //Set the schedule function and rate
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                updateUIHandler.post(updateUIRunnable);
            }
        },0,4000);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Create event listener
        View.OnClickListener hostGameListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                host(getContext());
            }
        };
        AdapterView.OnItemClickListener joinGameListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String[] data = parent.getAdapter().getItem(position).toString().split(" ");
                join(getContext(), data[0], data[1]);

            }
        };
        // Create button
        Button btnHost = view.findViewById(R.id.btn_lobby);
        btnHost.setOnClickListener(hostGameListener);
        // Create list
        ListView listJoin = view.findViewById(R.id.lv_lobby);
        listJoin.setOnItemClickListener(joinGameListener);
        listAdapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                R.layout.list_item,
                listItems);
        listJoin.setAdapter(listAdapter);
    }

    // Add to hostlist <-- now bufferlist that update hostlist in interval
    // Called by broadcast receiver
    public void updateHostList(String hostname, String hostip){
        Log.d(TAG, hostip + " " + hostname);
        if(!bufferList.contains(hostip + " " + hostname)){
            // Add to list
            // Add to buffered list an add them in interval to item list
            bufferList.add(hostip + " " + hostname);
        }
    }

    // Write buffered list to item list and renew the buffered list
    // Do this to clear old hosts
    public void checkLists(){
        // Remove old hosts
        // For each Host
        for(int j = 0; j < listItems.size(); j++){
            // Check each new host
            // Remove old host if he is not in current bufferedList
            if(!bufferList.contains(listItems.get(j))){
                listAdapter.remove(listItems.get(j));
            }
        }
        // Add new hosts
        for(int k = 0; k < bufferList.size(); k++){
            if(!listItems.contains(bufferList.get(k))){
                listAdapter.add(bufferList.get(k));
            }
        }
        // Clear new host list
        bufferList.clear();
    }

    private void switchFragment() {
        FragmentChangeListener fc = (FragmentChangeListener) getActivity();
        if (fc != null) {
            fc.replaceFragment(new WlanLobbyFragment());
        }
    }

    private void host(Context context){
        t.cancel();
        // Set you to host
        Player player = DAO.getInstance().getSelf();
        player.setHost(true);
        // Stop Service
        context.unregisterReceiver(this.hostReceiver);
        getActivity().getApplicationContext().stopService(i);

        // Start and trigger the Wlan service to send broadcast for hosting
        i = new Intent(context, BroadcastService.class);
        // add data to the intent for the service
        i.putExtra("IP", player.getConnectionID());
        i.putExtra("Name", player.getName());
        i.putExtra("IsHost", "true");
        getActivity().getApplicationContext().startService(i);

        // Wlan server
        WlanServer server = new WlanServer(2298);
        server.start();
        DAO.getInstance().setWlanServer(server);

        // Switch fragment
        switchFragment();
    }

    private void join(Context context, String IP, String Name){
        t.cancel();
        // Stop Service
        context.unregisterReceiver(this.hostReceiver);
        getActivity().getApplicationContext().stopService(i);

        // Set you to host
        Player player = DAO.getInstance().getSelf();
        player.setHost(false);

        // Wlan client
        WlanClient client = new WlanClient(IP,2298, player);
        client.start();
        DAO.getInstance().setWlanClient(client);

        // Switch fragment
        switchFragment();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Objects.requireNonNull(getContext()).unregisterReceiver(this.hostReceiver);
        t.cancel();
        // Unregister receiver
        getActivity().unregisterReceiver(this.wifiReceiver);
    }
}
