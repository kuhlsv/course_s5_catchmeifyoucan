package de.hsf.g4.catchmeifyoucan.wlan;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class BroadcastService extends Service {

    private static final String TAG = BroadcastService.class.getSimpleName();
    private static final int timeoutMS = 1000;
    private AsyncTask<Void, Void, Void> asyncIP;
    private AsyncTask<Void, Void, Void> asyncBroadcast;
    private boolean Server_aktiv = false;
    public String Message;
    public InetAddress broadcastAddress;
    public int serverPort = 8080; //8080
    private String playerIP;
    private Boolean playerIsHost;

    public BroadcastService(){ super(); }

    // This action processed on service start
    // On start he get the IP-Address by async task.
    // Loop try to get the address until he got one
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Get data from activity
        String playerName = intent.getStringExtra("Name");
        this.playerIP = intent.getStringExtra("IP");
        try{
            this.playerIsHost = Boolean.parseBoolean(intent.getStringExtra("IsHost"));
        }catch (Exception e){
            Log.e(TAG, "Unknown Host value!");
        }
        // Get the broadcast address from device in loop
        getBroadcastAddress();
        // Define Message
        Message = "OpenGame_CatchMeIfYouCan_" + playerName + "_" + playerIP;
        // Check if the player is host or going to join
        if(playerIsHost){
            if(Server_aktiv){
                // Reset receive and start send
                stopServer();
                stopBroadcast();
                asyncBroadcast = null;
            }
            this.startServer();
            // Send broadcast in loop
            sendBroadcast();
        }else{
            this.startServer();
            // Receiver broadcast in loop
            receiveBroadcast();
        }
        return Service.START_NOT_STICKY;
    }

    // Send the wlan broadcast if he got the ip address
    // To send the host data on open a game
    @SuppressLint({"NewApi", "StaticFieldLeak"})
    private void sendBroadcast() {
        // Set player ip to broadcast address if null
        if(this.broadcastAddress == null){
            try {
                // If there is no broadcast address, it show device ip
                this.broadcastAddress = InetAddress.getByName(playerIP);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        try {
            // Send broadcast
            asyncBroadcast = new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    while(Server_aktiv) { // Loop server is enabled
                        try (DatagramSocket ds = new DatagramSocket()) {
                            DatagramPacket dp;
                            dp = new DatagramPacket(Message.getBytes(), Message.length(), broadcastAddress, serverPort);
                            ds.setBroadcast(true);
                            Log.d(TAG, "Send");
                            ds.send(dp);
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            e.printStackTrace();
                        }
                        try {
                            Thread.sleep(timeoutMS); // Sleep
                        } catch (InterruptedException e) {
                            Log.e(TAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    return null;
                }

                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                }

            };

            asyncBroadcast.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
            try {
                Thread.sleep(timeoutMS);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    // Send the wlan broadcast if he got the ip address
    // To send the host data on open a game
    @SuppressLint({"NewApi", "StaticFieldLeak"})
    private void receiveBroadcast() {
        try {
            // Send broadcast
            asyncBroadcast = new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    while(Server_aktiv) { // Loop server is enabled
                        try (DatagramSocket ds = new DatagramSocket(serverPort, broadcastAddress)) {
                            // Get data
                            byte[] recvBuf = new byte[1024];
                            DatagramPacket dp;
                            dp = new DatagramPacket(recvBuf, recvBuf.length);
                            Log.d(TAG, "Receiver");
                            ds.receive(dp); // Receive network broadcast
                            String req = new String(dp.getData(), 0, dp.getLength());
                            Log.d(TAG, "data: " + req);
                            // Process data
                            String[] data = req.split("_");
                            if(data[0].equals("OpenGame")){
                                if(data[1].equals("CatchMeIfYouCan")){
                                    String hostName = data[2];
                                    String hostIP = data[3];
                                    // Send broadcast intent
                                    Intent intent = new Intent("BroadcastService.FoundHost");
                                    intent.putExtra("Name", hostName);
                                    intent.putExtra("IP", hostIP);
                                    sendBroadcast(intent);
                                }
                            }
                        } catch (Exception e) {
                            if(e.getMessage() != null){
                                Log.e(TAG, e.getMessage());
                            }
                            e.printStackTrace();
                        }
                        try {
                            Thread.sleep(timeoutMS); // Sleep
                        } catch (InterruptedException e) {
                            if(e.getMessage() != null){
                                Log.e(TAG, e.getMessage());
                            }
                            e.printStackTrace();
                        }
                    }
                    return null;
                }

                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                }
            };

            asyncBroadcast.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
            try {
                Thread.sleep(timeoutMS);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void getBroadcastAddress(){
        asyncIP = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                // Get the Broadcast IP Address.
                // If there is none, he will wait and loop for it again.
                Log.d(TAG,"Get broadcast address");
                getAddress();
                // Wait if he got an address or try again
                while(broadcastAddress == null){
                    try {
                        Log.d(TAG,"Wait");
                        Thread.sleep(timeoutMS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    getAddress();
                }
                // Cancel after got an address to access next async task
                asyncIP.cancel(true);
                Log.d(TAG, broadcastAddress.toString());
                return null;
            }

            // Get broadcast address
            private void getAddress() {
                WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                DhcpInfo dhcp;
                byte[] quads = null;
                if (wifi != null) {
                    dhcp = wifi.getDhcpInfo();
                    quads = new byte[4];
                    int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
                    for (int k = 0; k < 4; k++) {
                        quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
                    }
                }
                try{
                    broadcastAddress = InetAddress.getByAddress(quads);
                }catch(Exception e){
                    try {
                        Log.d(TAG,"No IP found.");
                        broadcastAddress = Inet4Address.getByName("255.255.255.0");
                    } catch (UnknownHostException e1) {
                        e1.printStackTrace();
                    }
                    Log.i(TAG, "Can not get broadcast address. Make sure ur Wlan is connected!");
                }
            }
        };

        asyncIP.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public boolean stopService(Intent name) {
        boolean valid = super.stopService(name);
        this.stopBroadcast();
        this.stopServer();
        this.asyncBroadcast.cancel(true);
        this.asyncIP.cancel(true);
        return valid;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.stopBroadcast();
        this.stopServer();
        this.asyncBroadcast.cancel(true);
        this.asyncIP.cancel(true);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void stopBroadcast(){
        asyncBroadcast.cancel(true);
    }

    public void restartBroadcast(){
        asyncBroadcast.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void stopServer() {
        Server_aktiv = false;
    }

    public void startServer() {
        Server_aktiv = true;
    }

}