package de.hsf.g4.catchmeifyoucan;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.util.Random;
import de.hsf.g4.catchmeifyoucan.db.DatabaseHelper;
import de.hsf.g4.catchmeifyoucan.sensors.Sensors;
import de.hsf.g4.catchmeifyoucan.ui.FragmentChangeListener;
import de.hsf.g4.catchmeifyoucan.ui.HomeFragment;
import de.hsf.g4.catchmeifyoucan.ui.MissingSensorFragment;
import de.hsf.g4.catchmeifyoucan.ui.NameDialogFragment;
import de.hsf.g4.catchmeifyoucan.wlan.ConnectionChangedReceiver;

public class MainActivity extends FragmentActivity implements FragmentChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private GameMode gamemode;
    private DatabaseHelper db;
    private Random r;

    public enum GameMode {
        Wlan, BT
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Player
        Player player;
        // Random
        this.r = new Random();
        // Read Player data from db
        this.db = new DatabaseHelper(this);
        Player dbPlayer = db.getPlayer();
        Boolean showNameDialog;
        if(dbPlayer == null){
            // Create and save Player
            player = new Player(Player.createID(r));
            this.db.addPlayer(player);
            showNameDialog = true;
        }else{
            player = dbPlayer;
            showNameDialog = !player.getNameIsSet();
        }
        DAO.getInstance().setSelf(player);
        // Show Fragments
        // Fragment and Layout: Home
        setContentView(R.layout.activity_main);
        replaceFragment(new HomeFragment());
        // Dialog
        if(showNameDialog){
            // Create Name Dialog
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            NameDialogFragment dialogFragment = new NameDialogFragment();
            dialogFragment.show(ft, "dialog");
        }

        // check if rotation vector sensor is available
        Sensors sensor = new Sensors(this);
        if(!sensor.getRotationVectorSensosrIsAvailability()) {
            replaceFragment(new MissingSensorFragment());
        }

    }

    @Override
    public void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_fragment, fragment, "content");
        fragmentTransaction.addToBackStack("content");
        fragmentTransaction.commit();
    }

    public GameMode getGamemode() {
        return gamemode;
    }

    public void setGamemode(GameMode gamemode) {
        this.gamemode = gamemode;
    }

     @Override
    public void onDestroy(){
        super.onDestroy();
        Player player = DAO.getInstance().getSelf();
        // Reset
        player.setPlayerRole(null);
        player.setHost(false);
        player.setConnectionID("");
        // Save Player data on exit
        this.db.updatePlayer(player);
        this.db = null;
    }

}
