package de.hsf.g4.catchmeifyoucan.wlan;

import android.util.Log;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.game.AtomicObstacle;
import de.hsf.g4.catchmeifyoucan.game.GyroXYValues;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;

public class WlanResponseHandler {

    private String TAG = "WlanResponsehandler";

    private WlanServer server;
    private WlanClient client;
    private WlanRequestBuilder rb = new WlanRequestBuilder();
    private DAO dao = DAO.getInstance();

    public WlanResponseHandler() {
    }

    public void setClient(WlanClient client) {
        this.client = client;
    }

    public void setServer(WlanServer server) {
        this.server = server;
    }

    public void serverHandle(String msg) {
        String splitter[] = msg.split(":");
        String type = splitter[0];
        String content = splitter[1];
        Player p = null;
        PlayerFigure pf = null;

        //TODO logic
        switch (type) {
            case "playerUpdate": // triggered at player update
                // save locally
                try {
                    p = (Player) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (p != null) {
                    ArrayList<Player> players = dao.getPlayers();
                    for (int i = 0; i < players.size(); i++) {
                        if (players.get(i).getPlayerID() == p.getPlayerID()) {
                            players.set(i, p);
                            Log.i("WlanResponsehandler", "Replaced player entry");
                        }
                    }
                    // reponse
                    String response = rb.build("players", players);
                    server.sendToAllClients(response);
                } else {
                    Log.i("WlanResponseHander", "player is null! Server will not respond!");
                }
                break;
            case "newPlayer": // new player joins
                try {
                    p = (Player) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ArrayList<Player> players = dao.getPlayers();
                players.add(p);
                dao.setPlayers(players);
                server.sendToAllClients(rb.build("players", players));
                break;
            case "playerGyro": // sends gyro object to server
                // save locally
                GyroXYValues xy = null;
                try {
                    xy = (GyroXYValues) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(xy != null) {
                    ArrayList<GyroXYValues> gyros = dao.getGyroValues();
                    boolean replaced = false;
                    try {
                        for (int i = 0; i < gyros.size(); i++) {
                            if (gyros.get(i).getPlayerID() == xy.getPlayerID()) {
                                gyros.set(i, xy);
                                Log.i(TAG, "Replaced gyroXY entry");
                                replaced = true;
                            }
                        }
                    }catch (Exception e){
                        Log.e(TAG,"Unknown Error.");
                    }
                    if (!replaced) {
                        Log.d(TAG,"added");
                        gyros.add(xy);
                    }
                    dao.setGyroValues(gyros);
                } else {
                    Log.e(TAG, "Malformed data!");
                }
                break;
            default:
                break;

        }
    }

    public void clientHandle(String msg) {
        String splitter[] = msg.split(":");
        String type = splitter[0];
        String content = splitter[1];
        ArrayList<Player> players = null;
        ArrayList<PlayerFigure> figures = null;

        //TODO logic
        switch (type) {
            case "players": // server sends playerArray to client
                try {
                    players = (ArrayList<Player>) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (players != null) {
                    dao.setPlayers(players);
                } else {
                    Log.i("WlanResponseHandler", "player arraylist is null!");
                }
                break;
            case "playerFigures": // server sends playerFigureArray to client
                try {
                    figures = (ArrayList<PlayerFigure>) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (figures != null) {
                    dao.setPlayerFigures(figures);
                } else {
                    Log.i("WlanResponseHandler", "playerfigure arraylist is null!");
                }
                break;
            case "obstacles":
                ArrayList<AtomicObstacle> obstacles = null;
                try {
                    obstacles = (ArrayList<AtomicObstacle>) rb.base64ToObject(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(obstacles != null) {
                    dao.setObstacles(obstacles);
                } else {
                    Log.e(TAG, "malformed obstacle data!");
                }
                break;
            case "start": // start signal
                dao.setStarted(true);
                break;
            default:
                break;

        }
    }


}
