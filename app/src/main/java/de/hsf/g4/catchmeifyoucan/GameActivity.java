package de.hsf.g4.catchmeifyoucan;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.game.AtomicObstacle;
import de.hsf.g4.catchmeifyoucan.game.Display;
import de.hsf.g4.catchmeifyoucan.game.GameEndDiagnostic;
import de.hsf.g4.catchmeifyoucan.game.GyroXYValues;
import de.hsf.g4.catchmeifyoucan.game.ObstacleSetter;
import de.hsf.g4.catchmeifyoucan.game.PlayerCoordinatorSimple;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;
import de.hsf.g4.catchmeifyoucan.sensors.Sensors;
import de.hsf.g4.catchmeifyoucan.wlan.ConnectionChangedReceiver;

import static de.hsf.g4.catchmeifyoucan.GameActivity.HierarchyType.MASTER;
import static de.hsf.g4.catchmeifyoucan.GameActivity.HierarchyType.SLAVE;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.FPS;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.GAMETIME;

public class GameActivity extends AppCompatActivity {

    enum HierarchyType {
        MASTER, SLAVE
    }

    private static final String TAG = "GameActivity";

    private ArrayList<AtomicObstacle> obstacleList;
    private Display display;

    private int ownPlayerID = 0;

    PlayerCoordinatorSimple playerCoordinator;
    private ConnectionChangedReceiver wifiReceiver;
    private Sensors movementSensor;

    private DAO dao = DAO.getInstance();

    private HierarchyType hierarchyType = dao.getSelf().getHost()?MASTER:SLAVE;

    ArrayList<Player.PlayerRole> playerRolesList = new ArrayList<Player.PlayerRole>();
    ArrayList<Integer> playerIDList = new ArrayList<Integer>();
    ArrayList<Player> playerList;

    private GameEndDiagnostic gameEndDiagnostic = new GameEndDiagnostic();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"GameActivity() started");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Wifi changed
        IntentFilter intentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        this.wifiReceiver = new ConnectionChangedReceiver();
        registerReceiver(this.wifiReceiver, intentFilter);
        movementSensor = new Sensors(this);
        movementSensor.instanceSensors();
        ownPlayerID = dao.getSelf().getPlayerID();

        if(hierarchyType ==  MASTER) {
            obstacleList = ObstacleSetter.createObstacles();
            display = new Display(this, obstacleList, ownPlayerID, movementSensor);
            playerList =  dao.getPlayers();
            for(Player player : playerList){
                Log.d("Game", "Player: " + player.toString());
                playerRolesList.add(player.getPlayerRole());
                playerIDList.add(player.getPlayerID());
            }
            playerCoordinator = new PlayerCoordinatorSimple(playerIDList, playerRolesList,obstacleList);
        } else { //Slave
            display = new Display(this, ownPlayerID, movementSensor);
        }

        final FrameLayout gameLayout = (FrameLayout)findViewById(R.id.layout);
        gameLayout.addView(display,0);

        final long startTime = System.currentTimeMillis();
        final TextView timeView = (TextView) gameLayout.findViewById(R.id.timeView);
        final TextView startCounterView = (TextView) gameLayout.findViewById(R.id.startCounter);
        final TextView winnerIsView = (TextView) gameLayout.findViewById(R.id.theWinnerIs);
        final TextView winnerView = (TextView) gameLayout.findViewById(R.id.winner);
        final long gameTime = GAMETIME * 1000; //ms

        final Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    final int startWaitSec = 3;
                    while (gameTime + startWaitSec * 1000 - (System.currentTimeMillis() - startTime) > 0 && !gameEndDiagnostic.isHunterWinner()) {
                        Thread.sleep(1000/FPS); // 20 fps
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(3*1000 - (System.currentTimeMillis() - startTime) > 0){
                                    //send ObstacleList
                                    if(hierarchyType ==  MASTER) {
                                        dao.setObstacles(obstacleList);
                                    } else {
                                        obstacleList = dao.getObstacles();
                                        display.setObstacleList(obstacleList);
                                    }

                                    long counter = startWaitSec - ((System.currentTimeMillis() - startTime) / 1000);
                                    startCounterView.setText(Long.toString(counter));
                                } else {
                                    //Log.d(TAG,"Game started");
                                    startCounterView.setText("");//clear Counter;
                                    float[] xy = movementSensor.getOrientationValues(); //get orientation values

                                    if (hierarchyType == MASTER) {//Master logic
                                        //Handle player positions
                                        ArrayList<GyroXYValues> gyroCords = dao.getGyroValues();
                                        gyroCords.add(new GyroXYValues((int)xy[0]*-1,(int)xy[1]*-1,ownPlayerID)); //adding own movement values
                                        playerCoordinator.setNewPosition(gyroCords);//calc new player positions
                                        gyroCords.clear();
                                        ArrayList<PlayerFigure> playerFigureList = playerCoordinator.getPlayerFigures();//get new player positions
                                        dao.setPlayerFigures(playerFigureList);//send new player positions to slaves
                                        display.setPlayerFiguresList(playerFigureList);//set new players in own display
                                        gameEndDiagnostic.gameEndDiagnosis(playerFigureList);//check if game is over
                                    } else { //Slave Logic
                                        dao.setGyroValue(new GyroXYValues((int)xy[0]*-1,(int)xy[1]*-1,ownPlayerID));//send own movement values
                                        ArrayList<PlayerFigure> playerFiguresList = dao.getPlayerFigures();//get player positions
                                        display.setPlayerFiguresList(playerFiguresList);//set player position in own display
                                        gameEndDiagnostic.gameEndDiagnosis(playerFiguresList);//check if game is over
                                    }

                                    long counter = (gameTime + startWaitSec * 1000 - (System.currentTimeMillis() - startTime)) / 1000;//calc game time counter
                                    timeView.setText(Long.toString(counter));//set game time counter
                                    display.invalidate();//update display
                                }

                            }
                        });

                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //show who is winner
                            if (gameEndDiagnostic.isHunterWinner()) {
                                winnerIsView.setText("Winners are");//Hunter is Winner
                                winnerView.setText("HUNTER");
                            } else {
                                winnerIsView.setText("Winners are");//Hunted is Winner
                                winnerView.setText("HUNTED");

                            }
                            display.invalidate();
                        }
                    });

                    //set own win counter
                    if (dao.getSelf().getPlayerRole() == Player.PlayerRole.HUNTER && gameEndDiagnostic.isHunterWinner()) {
                        dao.getSelf().setTotalWins(dao.getSelf().getTotalWins() + 1);
                    }
                    if (dao.getSelf().getPlayerRole() == Player.PlayerRole.HUNTED && !gameEndDiagnostic.isHunterWinner()) {
                        dao.getSelf().setTotalWins(dao.getSelf().getTotalWins() + 1);
                    }
                    dao.updatePlayer(dao.getSelf());

                    // Safe player to DB
                    Thread.sleep(1000);
                    DAO.getInstance().saveSelf(getApplicationContext());

                    // Close Activity
                    Thread.sleep(4000);
                    finish();

                    // Closed after 5 sec

                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        // Reset
        // Unregister receiver
        unregisterReceiver(this.wifiReceiver);
        wifiReceiver = null;
        display = null;
        dao = null;
    }

    @Override
    public void onBackPressed() {
        // do nothing
        return;
    }

}
