package de.hsf.g4.catchmeifyoucan.game;

import android.util.Log;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.Player;

import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_WALL_LENGTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_X;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_Y;

public class GameEndDiagnostic {
    private static String TAG = "GameEndDiagnostic";

    public boolean isHunterWinner() {
        return hunterWinner;
    }

    private boolean hunterWinner = false;

    public GameEndDiagnostic(){

    }

    public void gameEndDiagnosis(ArrayList<PlayerFigure> playerFigures){
        boolean atLeastOneIsNotInPrison = false;
        if(playerFigures.size() <= 0) {
            atLeastOneIsNotInPrison = true;
        } else {
            for (PlayerFigure playerFigure : playerFigures) {
                if (playerFigure.getPlayerRole() == Player.PlayerRole.HUNTED) {
                    if ((playerFigure.getXCoord() > PRISON_X && playerFigure.getXCoord() < PRISON_X + PRISON_WALL_LENGTH) &&
                            (playerFigure.getYCoord() > PRISON_Y && playerFigure.getYCoord() < PRISON_Y + PRISON_WALL_LENGTH)) {
                        //is in Prison
                    } else {
                        //is NOT in Prison
                        atLeastOneIsNotInPrison = true;
                    }
                }
            }
        }
        hunterWinner = !atLeastOneIsNotInPrison;
    }
}
