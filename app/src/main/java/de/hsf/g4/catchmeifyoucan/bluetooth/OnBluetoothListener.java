package de.hsf.g4.catchmeifyoucan.bluetooth;

public interface OnBluetoothListener {
    public void onReceived(String str);
    public void onStateChanged(String strState);
    public void onActiveChanged(Boolean serverInfo);
}
