package de.hsf.g4.catchmeifyoucan.game;

import java.util.ArrayList;

import static de.hsf.g4.catchmeifyoucan.game.GameProperties.AMOUNT_OF_OBSTACLE;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_HEIGHT;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MAP_WIDTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.MARGIN_OF_OBSTACLE;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_MAX_LENGTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_MIN_LENGTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.OBSTACLE_WALL_THICKNESS;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_WALL_LENGTH;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_WALL_THICKNESS;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_X;
import static de.hsf.g4.catchmeifyoucan.game.GameProperties.PRISON_Y;

public class ObstacleSetter {

    public static ArrayList<AtomicObstacle> createObstacles() {
        ArrayList<AtomicObstacle> obstacleList = new ArrayList<>();

        //set prison
        AtomicObstacle prisonWallLeft = new AtomicObstacle(PRISON_X, PRISON_Y, PRISON_X + PRISON_WALL_THICKNESS, PRISON_Y + PRISON_WALL_LENGTH, AtomicObstacle.ObstaclePartOfType.Prison);
        AtomicObstacle prisonWallTop = new AtomicObstacle(PRISON_X, PRISON_Y, PRISON_X + PRISON_WALL_LENGTH, PRISON_Y + PRISON_WALL_THICKNESS, AtomicObstacle.ObstaclePartOfType.Prison);
        AtomicObstacle prisonWallRight = new AtomicObstacle(PRISON_X + PRISON_WALL_LENGTH - PRISON_WALL_THICKNESS, PRISON_Y, PRISON_X + PRISON_WALL_LENGTH, PRISON_Y + PRISON_WALL_LENGTH, AtomicObstacle.ObstaclePartOfType.Prison);
        AtomicObstacle prisonWallBottom = new AtomicObstacle(PRISON_X, PRISON_Y + PRISON_WALL_LENGTH - PRISON_WALL_THICKNESS, PRISON_X + PRISON_WALL_LENGTH, PRISON_Y + PRISON_WALL_LENGTH, AtomicObstacle.ObstaclePartOfType.Prison);
        obstacleList.add(prisonWallLeft);
        obstacleList.add(prisonWallTop);
        obstacleList.add(prisonWallRight);
        obstacleList.add(prisonWallBottom);

        for(int i = 0; i < AMOUNT_OF_OBSTACLE; i++) {
            boolean notPlaced = true;
            switch (randomNum(1,4)){
                case 1: //Wall horizontal
                    for(int k = 0; k < 30 && notPlaced; k++) {
                        int length = randomNum(OBSTACLE_WALL_MIN_LENGTH, OBSTACLE_WALL_MAX_LENGTH);
                        int thickness = OBSTACLE_WALL_THICKNESS;
                        int x = randomNum(0, MAP_WIDTH - length);
                        int y = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_HEIGHT - thickness - MARGIN_OF_OBSTACLE);

                        //Get Wall to World Border if distance lower than MARGIN_OF_OBSTACLE
                        if(x < MARGIN_OF_OBSTACLE) {
                            x = 0;
                        }
                        if(MAP_WIDTH - (x + length) < MARGIN_OF_OBSTACLE) {
                            x = MAP_WIDTH - length;
                        }

                        AtomicObstacle newObstacle = new AtomicObstacle(x, y, x + length, y + thickness, AtomicObstacle.ObstaclePartOfType.WallHor);
                        boolean isNotCollide = true;
                        for (int j = 0; j < obstacleList.size() && isNotCollide; j++) {
                            isNotCollide = !Crasher.isCrashedObstacles(obstacleList.get(j), newObstacle, MARGIN_OF_OBSTACLE);
                        }
                        if (isNotCollide) {
                            obstacleList.add(newObstacle);
                            notPlaced = false;
                        }

                    }
                    break;

                case 2: //Wall vertical
                    for(int k = 0; k < 30 && notPlaced; k++) {
                        int length = randomNum(OBSTACLE_WALL_MIN_LENGTH, OBSTACLE_WALL_MAX_LENGTH);
                        int thickness = OBSTACLE_WALL_THICKNESS;
                        int x = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_WIDTH - thickness - MARGIN_OF_OBSTACLE);
                        int y = randomNum(0, MAP_HEIGHT - length);

                        //Get Wall to World Border if distance lower than MARGIN_OF_OBSTACLE
                        if(y < MARGIN_OF_OBSTACLE) {
                            y = 0;
                        }
                        if(MAP_HEIGHT - (y + length) < MARGIN_OF_OBSTACLE) {
                            y = MAP_HEIGHT - length;
                        }

                        AtomicObstacle newObstacle = new AtomicObstacle(x, y, x + thickness, y + length, AtomicObstacle.ObstaclePartOfType.WallVer);
                        boolean isNotCollide = true;
                        for (int j = 0; j < obstacleList.size() && isNotCollide; j++) {
                            isNotCollide = !Crasher.isCrashedObstacles(obstacleList.get(j), newObstacle, MARGIN_OF_OBSTACLE);
                        }
                        if (isNotCollide) {
                            obstacleList.add(newObstacle);
                            notPlaced = false;
                        }

                    }
                    break;

                case 3: // Cross
                    for(int k = 0; k < 30 && notPlaced; k++) {
                        int length = randomNum(OBSTACLE_WALL_MIN_LENGTH, OBSTACLE_WALL_MAX_LENGTH);
                        int thickness = OBSTACLE_WALL_THICKNESS;
                        int x = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_WIDTH - length - MARGIN_OF_OBSTACLE);
                        int y = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_HEIGHT - length - MARGIN_OF_OBSTACLE);

                        int wallVerticalX = x + (length/2) - (thickness/2);
                        int wallVerticalY = y;
                        int wallHorizontalX = x;
                        int wallHorizontalY = y + (length/2) - (thickness/2);

                        AtomicObstacle newObstacle1 = new AtomicObstacle(wallVerticalX, wallVerticalY, wallVerticalX + thickness, wallVerticalY + length, AtomicObstacle.ObstaclePartOfType.Cross);
                        AtomicObstacle newObstacle2 = new AtomicObstacle(wallHorizontalX, wallHorizontalY, wallHorizontalX + length, wallHorizontalY + thickness, AtomicObstacle.ObstaclePartOfType.Cross);

                        boolean isNotCollide1 = true;
                        boolean isNotCollide2 = true;
                        for (int j = 0; j < obstacleList.size() && isNotCollide1 && isNotCollide2; j++) {
                            isNotCollide1 = !Crasher.isCrashedObstacles(obstacleList.get(j), newObstacle1, MARGIN_OF_OBSTACLE);
                            isNotCollide2 = !Crasher.isCrashedObstacles(obstacleList.get(j), newObstacle2, MARGIN_OF_OBSTACLE);
                        }
                        if (isNotCollide1 && isNotCollide2) {
                            obstacleList.add(newObstacle1);
                            obstacleList.add(newObstacle2);
                            notPlaced = false;
                        }
                    }
                    break;

                case 4: //Corner
                    for(int k = 0; k < 30 && notPlaced; k++) {
                        int length = randomNum(OBSTACLE_WALL_MIN_LENGTH, OBSTACLE_WALL_MAX_LENGTH);
                        int thickness = OBSTACLE_WALL_THICKNESS;
                        int x = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_WIDTH - length - MARGIN_OF_OBSTACLE);
                        int y = randomNum(0 + MARGIN_OF_OBSTACLE, MAP_HEIGHT - length - MARGIN_OF_OBSTACLE);

                        int wallHorizontalX;
                        int wallHorizontalY;
                        int wallVerticalX;
                        int wallVerticalY;

                        switch (randomNum(1,4)){
                            //rotation 0°
                            case 1:
                                wallHorizontalX = x;
                                wallHorizontalY = y;
                                wallVerticalX = x;
                                wallVerticalY = y;
                                break;

                            //rotation 90°
                            case 2:
                                wallHorizontalX = x;
                                wallHorizontalY = y;
                                wallVerticalX = x + length - thickness;
                                wallVerticalY = y;
                                break;

                            //rotation 180°
                            case 3:
                                wallHorizontalX = x;
                                wallHorizontalY = y + length - thickness;
                                wallVerticalX = x + length - thickness;
                                wallVerticalY = y;
                                break;

                            //rotation 270°
                            case 4:
                                wallHorizontalX = x;
                                wallHorizontalY = y + length - thickness;
                                wallVerticalX = x;
                                wallVerticalY = y;
                                break;

                            default:
                                wallHorizontalX = x;
                                wallHorizontalY = y;
                                wallVerticalX = x;
                                wallVerticalY = y;
                        }

                        AtomicObstacle newObstacle1 = new AtomicObstacle(wallHorizontalX, wallHorizontalY, wallHorizontalX + length, wallHorizontalY + thickness, AtomicObstacle.ObstaclePartOfType.Corner);
                        AtomicObstacle newObstacle2 = new AtomicObstacle(wallVerticalX, wallVerticalY, wallVerticalX + thickness, wallVerticalY + length, AtomicObstacle.ObstaclePartOfType.Corner);

                        boolean isNotCollide1 = true;
                        boolean isNotCollide2 = true;
                        for (int j = 0; j < obstacleList.size() && isNotCollide1 && isNotCollide2; j++) {
                            isNotCollide1 = !Crasher.isCrashedObstacles(obstacleList.get(j),newObstacle1, MARGIN_OF_OBSTACLE);
                            isNotCollide2 = !Crasher.isCrashedObstacles(obstacleList.get(j),newObstacle2, MARGIN_OF_OBSTACLE);
                        }
                        if (isNotCollide1 && isNotCollide2) {
                            obstacleList.add(newObstacle1);
                            obstacleList.add(newObstacle2);
                            notPlaced = false;
                        }
                    }
                    break;

                default:

            }
        }

        return obstacleList;
    }

    private static int randomNum(int min, int max){
        return (int) (Math.random() * (max+1 - min)) + min;
    }
}
