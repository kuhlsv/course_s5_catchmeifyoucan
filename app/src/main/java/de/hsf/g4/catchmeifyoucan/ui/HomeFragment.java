package de.hsf.g4.catchmeifyoucan.ui;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Objects;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.MainActivity;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.wlan.Utils;

public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private String IP;
    private String MAC;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Create event listener
        View.OnClickListener multiplayerClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToWlanFragment();
            }
        };

        View.OnClickListener bluetoothClickListener =  new View.OnClickListener(){
            @Override
            public void onClick(View v){
                switchToBluetoothFragment();
            }
        };
        // Twoplayer button
        Button btnS = view.findViewById(R.id.buttonS);
        btnS.setOnClickListener(bluetoothClickListener);
        ImageView ivS = view.findViewById(R.id.imageBluetooth);
        ivS.setOnClickListener(bluetoothClickListener);
        // Multiplayer button
        Button btnM = view.findViewById(R.id.buttonM);
        btnM.setOnClickListener(multiplayerClickListener);
        ImageView ivM = view.findViewById(R.id.imageWlan);
        ivM.setOnClickListener(multiplayerClickListener);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void switchToWlanFragment() {
        Player player = DAO.getInstance().getSelf();
        this.IP = Utils.getIPAddress(true);
        this.MAC = Utils.getMACAddress("wlan0");
        if(Utils.checkWlan(Objects.requireNonNull(getContext())) && !this.IP.equals("")){
            // Set Player id to IP
            player.setConnectionID(this.IP);
            // Set Game mode
            ((MainActivity)getActivity()).setGamemode(MainActivity.GameMode.Wlan);
            // Switch Fragment
            FragmentChangeListener fc = (FragmentChangeListener) getActivity();
            if (fc != null) {
                fc.replaceFragment(new OverviewFragment());
            }
        }else{
            // Force to enable Wifi
            //Utils.forceSettings(getContext());
            CharSequence text = "Something went wrong. Make sure ur Wlan is connected!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(this.getContext(), text, duration);
            toast.show();
        }
    }

    public void switchToBluetoothFragment() {
        // TODO Save BT MAC to PLAYER !
        ((MainActivity)getActivity()).setGamemode(MainActivity.GameMode.BT);
        FragmentChangeListener fc = (FragmentChangeListener) getActivity();
        if (fc != null) {
            fc.replaceFragment(new BluetoothLobbyFragment());
        }
    }
}
