package de.hsf.g4.catchmeifyoucan.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import de.hsf.g4.catchmeifyoucan.DAO;
import de.hsf.g4.catchmeifyoucan.GameActivity;
import de.hsf.g4.catchmeifyoucan.Player;
import de.hsf.g4.catchmeifyoucan.R;
import de.hsf.g4.catchmeifyoucan.bluetooth.BluetoothController;
import de.hsf.g4.catchmeifyoucan.bluetooth.Controller;
import de.hsf.g4.catchmeifyoucan.bluetooth.OnBluetoothListener;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;
import de.hsf.g4.catchmeifyoucan.wlan.BroadcastService;

public class BluetoothLobbyFragment extends Fragment implements View.OnClickListener,
        OnBluetoothListener {
    // region Variables
    private static final String TAG = "hsflBluetoothLobby";
    int duration = Toast.LENGTH_SHORT;
    private Context context;
    private Button findBtn,hostBtn,hunterBtn,huntedBtn,readyBtn;
    private TextView text;
    private BluetoothAdapter btAdapt;
    private Set<BluetoothDevice> pairedDev;
    private BluetoothDevice btTempDevice;
    private ListView list;
    private ArrayAdapter<String> BTArrayAdapter;
    private static final int REQUEST_ENABLE_BT = 1;
    private ArrayList<BluetoothDevice> foundDeviceList = new ArrayList<>();
    private String macAddress = "";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FC");
    private final String SERVICE = "SerialPort";
    private BluetoothController btController;
    public static final int REQUEST_DISCOVERABLE_CODE = 2;
    private DAO dao = DAO.getInstance();
    private Player.PlayerRole role;
    private boolean isHost = false;
    private boolean isReady = false;
    // endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bluetooth_lobby, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        btAdapt = BluetoothAdapter.getDefaultAdapter();
        text = (TextView) getView().findViewById(R.id.text);

        IntentFilter btStateChanged = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        context.registerReceiver(bReceiver,btStateChanged);
        btController = Controller.getInstance();
        btController.setListener(this);

        /**
         * Check if the Device supports Bluetooth
         */
        if (btAdapt == null) {
            text.setText("Status: BlueTooth is not supported!");
            makeToast("Your device does not support Bluetooth");
        } else {
            /**
             * Initialize the OnClick Listeners and Status text
             */
            text.setText("Status: BlueTooth active..");
            on(view);
            hostBtn = (Button) getView().findViewById(R.id.btn_host);
            hostBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if(btAdapt.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
                    {
                        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                        intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120);
                        startActivityForResult(intent, REQUEST_DISCOVERABLE_CODE);
                    }
                    enableUIelements("Join",false);
                    hostBtn.setEnabled(false);
                    isHost = true;
                    hostGame();
                }
            });
            findBtn = (Button) getView().findViewById(R.id.search);
            findBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    //enableRoleButtons("chooseRoleActive");
                    enableUIelements("List",true);
                    enableUIelements("Host",false);
                    isHost = false;
                    find(v);

                }
            });

            list = (ListView) getView().findViewById(R.id.listView1);
            list.setOnItemClickListener(mDeviceClickListener);

            hunterBtn = getView().findViewById(R.id.roleHunter);
            hunterBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Send the Role to corresponding Device
                    //HUNTER (enum)
                    isReady = true;
                    btController.send("Hunter");
                    enableUIelements("Hunted",false);
                    hunterBtn.setEnabled(false);
                    role = Player.PlayerRole.HUNTER;
                    updatePlayer();
                    btController.sendPlayer(dao.getSelf());
                    //hunterBtn

                }
            });
            huntedBtn = getView().findViewById((R.id.roleHunted));
            huntedBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Send the Role to corresponding Device
                    //HUNTED (enum)
                    isReady = true;
                    btController.send("Hunted");
                    enableUIelements("Hunter",false);
                    huntedBtn.setEnabled(false);
                    role = Player.PlayerRole.HUNTED;
                    updatePlayer();
                    Log.d("HUNTED", "hunted btn");

                    btController.sendPlayer(dao.getSelf());



                }
            });
            readyBtn = getView().findViewById(R.id.btnReady);
            readyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btController.sendPlayer(dao.getSelf());
                    dao.startGame();
                    if(dao.isStarted()){
                        Intent i = new Intent(context, BroadcastService.class);
                        getActivity().getApplicationContext().stopService(i);
                        Intent intent = new Intent(getActivity(), GameActivity.class);
                        getActivity().startActivity(intent);
                    }
                    btController.send("start");
                }
            });

            /**
             * create the arrayAdapter that contains the BTDevices, and set it to the ListView
             */
            BTArrayAdapter = new ArrayAdapter<String>(context, R.layout.list_item);
            list.setAdapter(BTArrayAdapter);


        }
    }

    /**
     * Turns the Bluetooth on
     */
    public void on(View view){
        if (!btAdapt.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);
            makeToast("Bluetooth turned on");

        }
        else{
            makeToast("Bluetooth is already on");
        }
    }

    /**
     * Turns Bluetoooth off
     */
    public void off(View view){
        btAdapt.disable();
        text.setText("Status: Disconnected");
        makeToast("Bluetooth turned off");
    }

    /**
     * Starts the Discovery and looks for nerby Visible BT-Devices
     */
    public void find(View view){
        if (btAdapt.isDiscovering()) {
            // the button is pressed when it discovers, so cancel the discovery
            btAdapt.cancelDiscovery();
            foundDeviceList.clear();
            Log.d(TAG,"Cancel Discovery");
        }
        else {
            Log.d(TAG,"start Discovery");
            BTArrayAdapter.clear();
            foundDeviceList.clear();
            text.setText("Status: Freunde werden gesucht...");

            checkBTpermission();
            btAdapt.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            context.registerReceiver(bReceiver, discoverDevicesIntent);

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if(requestCode == REQUEST_ENABLE_BT){
            if(btAdapt.isEnabled()) {
                text.setText("Status: Enabled");
            } else {
                text.setText("Status: Disabled");
            }
        }
    }

    /**
     * BroadcastReceiver to Check if devices were found // the BT status changed
     */
    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            //region Handles Receiver
            String action = intent.getAction();
            Log.d(TAG, "OnRecieve");

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                foundDeviceList.add(device);
                Log.d(TAG,"ArrayList: "+foundDeviceList.size());

                // add the name and the MAC address of the object to the arrayAdapter
                BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                Log.d(TAG, "devices: "+device.getName() + " "+device.getAddress());
                BTArrayAdapter.notifyDataSetChanged();
                text.setText("Status: Wähle einen Freund aus");
            }
            if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)){
                if (checkForBond()) {
                    join(macAddress);
                }

            }
            // endregion
        }
    };

    /**
     * Returns the MacAdress of the Connected Client
     */
    public String getMac(){
        return this.macAddress;
    }

    /**
     * Checks if BT permission is enabled and Requests it
     */
    @SuppressLint("NewApi")
    public void checkBTpermission(){
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = context.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATON");
            permissionCheck += context.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if(permissionCheck != 0){
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission
                        .ACCESS_COARSE_LOCATION}, 1001);
            }
        }
    }

    /**
     * The on-click listener for all devices in the ListViews
     */
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int position, long arg3) {

            // region handles ItemClicks in ListView
            Log.d(TAG, "ListViewOnClick");

            // Cancel discovery because it's costly and we're about to connect
            btAdapt.cancelDiscovery();
            btTempDevice = foundDeviceList.get(position);
            macAddress = " ";
            if (!checkForBond()) {
                try {
                    createBond(btTempDevice);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            macAddress = info.substring(info.length() - 17);
            Log.d(TAG, macAddress);

            if(checkForBond()){
                Log.d(TAG,"isAlreadyPaired");
                join(macAddress);
            }
            // endregion
        }
    };

    public boolean createBond(BluetoothDevice btDevice) throws Exception
    {
        Log.d(TAG,"makeBond");
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }

    /**
     * Makes UserInformations
     */
    public void makeToast(String toastText){
        Toast.makeText(context,toastText,duration).show();
    }

    /**
     * Sets the APP to Host mode
     */
    public void hostGame(){
        text.setText("Status: Warte auf Spieler");
        BTArrayAdapter.clear();
        btController.initAsServer(getActivity(), MY_UUID, SERVICE, true );
        btController.start();
        text.setText("Status: Spieler beigetreten");
        enableUIelements("List", false);
    }

    /**
     * Sets the APP to ClientMode
     */
    public void join(String btServerAddress){
        Log.d(TAG,"joinServer");
        text.setText("Status: Trete spiel bei");
        btController.initAsClient(getActivity(), btServerAddress, MY_UUID ,SERVICE, true);
        btController.start();
        text.setText("Status: Spiel beigetreten");
        //reset lobby ui
        enableUIelements("List", false);
    }

    /**
     * Checks if a device is Properly bonded
     */
    public boolean checkForBond(){
        // get paired devices
        pairedDev = btAdapt.getBondedDevices();
        // put it's one to the adapter
        for(BluetoothDevice device : pairedDev){
            Log.d(TAG,"Paired Device"+device.getAddress());
            if(device.getAddress().equals(macAddress)){
                return true;
            }
        }
        return false;
    }
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        context.unregisterReceiver(bReceiver);
    }


    /**
     * Enables UI elements
     */
    public void enableUIelements(String uiSatus, boolean enable){
        switch (uiSatus){
            case "RoleButtons":
                if (enable){
                    text.setText("Rolle wählen");
                    huntedBtn.setVisibility(View.VISIBLE);
                    hunterBtn.setVisibility(View.VISIBLE);
                }
                else{
                    huntedBtn.setVisibility(View.GONE);
                    hunterBtn.setVisibility(View.GONE);
                }
                break;
            case "Host":
                if(enable)
                    hostBtn.setVisibility(View.VISIBLE);
                else
                    hostBtn.setVisibility(View.GONE);
                break;
            case "Join":
                if(enable)
                    findBtn.setVisibility(View.VISIBLE);
                else
                    findBtn.setVisibility(View.GONE);
                break;
            case"Hunter":
                if (enable){
                    hunterBtn.setVisibility(View.VISIBLE);
                }
                else{
                    hunterBtn.setVisibility(View.GONE);

                }
                break;
            case "Hunted":
                if(enable){
                    huntedBtn.setVisibility(View.VISIBLE);
                }
                else{
                    huntedBtn.setVisibility(View.GONE);
                }
                break;
            case "List":
                if(enable){
                    list.setVisibility(View.VISIBLE);
                }
                else{
                    list.setVisibility(View.GONE);
                }
                break;
            case "Ready":
                if(enable){
                    readyBtn.setVisibility(View.VISIBLE);
                }
                else{
                    readyBtn.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public void onReceived(String str) {
        Log.d(TAG,"MessageToLobby " + str);
        switch (str){
            case"Hunter":
                enableUIelements("Hunter",false);
                if(isHost)
                    enableUIelements("Ready", true);
                break;
            case"Hunted":
                enableUIelements("Hunted",false);
                if(isHost)
                    enableUIelements("Ready", true);
                break;
            case "start":
                this.dao.startGame();
                Log.d(TAG, "Start received");
                if(this.dao.isStarted()){
                    Intent i = new Intent(context, BroadcastService.class);
                    getActivity().getApplicationContext().stopService(i);
                    Intent intent = new Intent(getActivity(), GameActivity.class);
                    getActivity().startActivity(intent);
                }
        }
    }

    @Override
    public void onStateChanged(String strState) {
        if(strState.equals("CONNECTED")){
            enableUIelements("RoleButtons", true);
            findBtn.setEnabled(false);
        }
    }

    @Override
    public void onActiveChanged(Boolean serverInfo) {

    }

    public void updatePlayer(){
        dao.getSelf().setPlayerRole(role);
        dao.getSelf().setHost(isHost);
        dao.getSelf().setReady(isReady);
        btController.sendPlayer(dao.getSelf());
    }

}
