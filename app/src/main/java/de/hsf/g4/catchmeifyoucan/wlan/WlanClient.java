package de.hsf.g4.catchmeifyoucan.wlan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import java.net.Socket;
import de.hsf.g4.catchmeifyoucan.Player;

public class WlanClient extends Thread {

    private final String TAG = "WlanClient";
    private String addr;
    private int port;
    private Player player;
    private BufferedWriter out;
    private BufferedReader in;
    private WlanRequestBuilder rb = new WlanRequestBuilder();
    private WlanResponseHandler rh = new WlanResponseHandler();

    public WlanClient(String addr, int port, Player p) {
        this.addr = addr;
        this.port = port;
        this.player = p;
        rh.setClient(this);
    }


    public void run() {
        Log.d(TAG, "starting...");
        Socket socket;
        try {
            socket = new Socket(this.addr, this.port);
            Log.i(TAG, "connected!");
            Log.i(TAG, "sending handshake...");
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String send = rb.build("newPlayer", player);
            sendToServer(send);
            // listener
            String fromServer;
            while ((fromServer = in.readLine()) != null) {
                rh.clientHandle(fromServer);
            }

        }
        catch(Exception e){
            Log.e("err", e.getMessage());
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public void sendToServer(String msg) {
        AsyncTask<String, Void, Void> send = new AsyncTask<String, Void, Void>() {

            @Override
            protected Void doInBackground(String... strings) {
                try {
                    //Log.i(TAG +" Sending", strings[0]);
                    out.write(strings[0]);
                    out.newLine();
                    out.flush();
                    Log.i(TAG, "Sent.");
                } catch(java.io.IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        send.execute(msg);
    }
}

