package de.hsf.g4.catchmeifyoucan;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import de.hsf.g4.catchmeifyoucan.bluetooth.BluetoothController;
import de.hsf.g4.catchmeifyoucan.bluetooth.Controller;
import de.hsf.g4.catchmeifyoucan.db.DatabaseHelper;
import de.hsf.g4.catchmeifyoucan.game.AtomicObstacle;
import de.hsf.g4.catchmeifyoucan.game.GyroXYValues;
import de.hsf.g4.catchmeifyoucan.game.PlayerFigure;
import de.hsf.g4.catchmeifyoucan.wlan.WlanClient;
import de.hsf.g4.catchmeifyoucan.wlan.WlanRequestBuilder;
import de.hsf.g4.catchmeifyoucan.wlan.WlanServer;

public class DAO {
    private static final String TAG = DAO.class.getSimpleName();
    private static DAO instance = null;
    private WlanClient wlanClient = null;
    private WlanServer wlanServer = null;
    private Player self = null;
    private boolean isStarted = false;
    private WlanRequestBuilder rb = new WlanRequestBuilder();
    private ArrayList<PlayerFigure> playerFigures = new ArrayList<>();
    private ArrayList<Player> players = new ArrayList<>();
    private ArrayList<AtomicObstacle> obstacles = new ArrayList<>();
    private ArrayList<GyroXYValues> gyroValues = new ArrayList<>();
    private static BluetoothController btController;

    private DAO() {
    }

    public static DAO getInstance() {
        if (instance == null){
            instance = new DAO();
            btController = Controller.getInstance();
        }
        return instance;
    }

    public ArrayList<PlayerFigure> getPlayerFigures() {
        //Log.d("hsflDAO", "" + playerFigures.size());
        return playerFigures;
    }

    public ArrayList<Player> getPlayers() {
        Log.d("hsflDAO", "" + players.size());
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public WlanClient getWlanClient() {
        return wlanClient;
    }

    public void setWlanClient(WlanClient client) {
        this.wlanClient = client;
    }

    public WlanServer getWlanServer() {
        return wlanServer;
    }

    public void setWlanServer(WlanServer server) {
        this.wlanServer = server;
    }

    // server to client
    public void setPlayerFigures(ArrayList<PlayerFigure> figs) {
        if(wlanClient != null) {
            playerFigures = figs;
        } else if(wlanServer != null) {
            // is wlan server
            playerFigures = figs;
            wlanServer.sendToAllClients(rb.build("playerFigures", figs));
        } else {
            // bluetooth
            playerFigures = figs;
            if(getSelf().getHost()){
                btController.sendPlayerFigures(figs);
            }
        }
    }

    public void updatePlayer(Player p) {
        // wlan client
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getPlayerID() == p.getPlayerID()) {
                players.set(i, p);
                if(wlanClient != null) {
                    wlanClient.sendToServer(rb.build("playerUpdate", p));
                } else if(wlanServer != null) {
                    wlanServer.sendToAllClients(rb.build("players", p));
                } else {
                    // bluetooth
                    btController.sendPlayer(p);
                }
            }
        }
    }

    public Player getSelf() {
        return self;
    }

    public void setSelf(Player self) {
        this.self = self;
    }

    public ArrayList<AtomicObstacle> getObstacles() {
        return obstacles;
    }

    public void setObstacles(ArrayList<AtomicObstacle> obstacles) {
        this.obstacles = obstacles;
        if(wlanServer != null) {
            // is wlan server
            wlanServer.sendToAllClients(rb.build("obstacles", obstacles));
        } else {
            // bluethooth
            if(getSelf().getHost()){
                btController.sendObstacles(obstacles);
            }
        }
    }

    public ArrayList<GyroXYValues> getGyroValues() {

        return gyroValues;
    }

    public void setGyroValues(ArrayList<GyroXYValues> gyroValues) {
        this.gyroValues = gyroValues;
    }

    public void setGyroValue(GyroXYValues gyroVals) {
        if(wlanClient != null) {
            // wlanclient
            wlanClient.sendToServer(rb.build("playerGyro", gyroVals));
        } else {
            // bluetooth
            if(!getSelf().getHost() && gyroVals != null){
                btController.sendGyroValues(gyroVals);
            }
        }
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean started) {
        isStarted = started;
    }

    public void startGame() { // host method

        if(wlanServer != null) {
            // wlan server
            isStarted = true;
            wlanServer.sendToAllClients(rb.build("start", "yee"));
        } else {
            // bluetooth server
            isStarted = true;
        }
    }

    public void saveSelf(Context context){
        DatabaseHelper db = new DatabaseHelper(context);
        db.updatePlayer(getSelf());
    }

}
