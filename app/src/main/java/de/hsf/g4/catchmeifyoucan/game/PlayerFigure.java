package de.hsf.g4.catchmeifyoucan.game;

import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import de.hsf.g4.catchmeifyoucan.Player;

public class PlayerFigure implements Serializable {

    private int xCoord = 0;
    private int yCoord = 0;

    private Player.PlayerRole playerRole;
    private int playerID; //use connection ip for this

    public PlayerFigure(int playerID, Player.PlayerRole playerRole){
        this.playerID = playerID;
        this.playerRole = playerRole;
    }

    public int getXCoord() {
        return xCoord;
    }

    public void setXCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    public int getYCoord() {
        return yCoord;
    }

    public void setYCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    public Player.PlayerRole getPlayerRole() {
        return playerRole;
    }

    public void setPlayerRole(Player.PlayerRole playerRole) {
        this.playerRole = playerRole;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }
}
