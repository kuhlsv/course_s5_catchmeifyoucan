package de.hsf.g4.catchmeifyoucan.game;

import java.io.Serializable;

public class AtomicObstacle implements Serializable {

    public enum ObstaclePartOfType {
        WallHor, WallVer, Corner, Cross, Prison
    }

    private int left;
    private int right;
    private int top;
    private int bottom;
    private ObstaclePartOfType obstaclePartOf;

    public AtomicObstacle(int left, int top, int right, int bottom) {
        this(left, top, right, bottom, ObstaclePartOfType.WallHor);
    }
    public AtomicObstacle(int left, int top, int right, int bottom, ObstaclePartOfType obstaclePartOf){
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.obstaclePartOf = obstaclePartOf;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public ObstaclePartOfType getObstaclePartOf() {
        return obstaclePartOf;
    }

    public void setObstaclePartOf(ObstaclePartOfType obstaclePartOf) {
        this.obstaclePartOf = obstaclePartOf;
    }
}
