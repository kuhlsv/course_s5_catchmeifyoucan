package de.hsf.g4.catchmeifyoucan.ui;

import android.support.v4.app.Fragment;

public interface FragmentChangeListener
{
    void replaceFragment(Fragment fragment);
}