//  Client Side Code: TestUDPClient.java
//  Syntax:  java TestUDPClient.java  remotehost  port

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
 
public class TestUDPClient
{
    public static void main(String str[]) throws IOException
    {
        String msg = "Hello";
        InetAddress group = InetAddress.getByName(str[0]);
        DatagramSocket s = new DatagramSocket();
        DatagramPacket hi = new DatagramPacket(msg.getBytes(), msg.length(), group, Integer.parseInt(str[1]));
         s.send(hi);
         // get their responses!
         byte[] buf = new byte[1000];
         DatagramPacket recv = new DatagramPacket(buf, buf.length);
         s.receive(recv);
         System.out.println(new String(buf));
    }
}